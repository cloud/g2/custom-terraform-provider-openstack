FROM ubuntu:jammy

COPY terraform/terraform /usr/local/bin
COPY terraform-provider-openstack .
RUN mkdir -p $HOME/.terraform.d/plugins/gitlab.ics.muni.cz/cloud/openstack/0.0.1/linux_amd64 && \
    mv terraform-provider-openstack $HOME/.terraform.d/plugins/gitlab.ics.muni.cz/cloud/openstack/0.0.1/linux_amd64/terraform-provider-openstack

# Terraform autocomplete
RUN touch ~/.bashrc && terraform -install-autocomplete

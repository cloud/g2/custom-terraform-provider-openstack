# Custom Terraform Provider Openstack

1. build binary
2. (windows) `env GOOS=windows GOARCH=amd64 go build -o terraform-provider-openstack.exe`
3. (linux) `env GOOS=linux GOARCH=amd64 go build -o terraform-provider-openstack`
4. move it to terraform plugin directory (https://developer.hashicorp.com/terraform/cli/config/config-file#implied-local-mirror-directories)
5. (windows) for example C:\Users\{USERNAME}\AppData\Roaming\terraform.d\plugins\gitlab.ics.muni.cz\cloud\openstack\0.0.1\windows_amd64\terraform-provider-openstack.exe
6. (linux) for example ~/.terraform.d/plugins/gitlab.ics.muni.cz/cloud/openstack/0.0.1/linux_amd64/terraform-provider-openstack
7. `terraform init`

```
$ terraform plan

Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
  + create                                                                                                                                

Terraform will perform the following actions:           
                                                        
  # openstack_domain.einfra_cz will be created          
  + resource "openstack_domain" "einfra_cz" {           
      + description = "e-INFRA.cz domain"               
      + enabled     = true                              
      + id          = (known after apply)               
      + name        = "einfra_cz"                       
    }                                                   
                                                        
  # openstack_federation_protocol.openid will be created
  + resource "openstack_federation_protocol" "openid" { 
      + id                   = (known after apply)
      + identity_provider_id = (known after apply)
      + mapping_id           = "mapping_1"
      + protocol_id          = "openid"
    }

  # openstack_identity_provider.login_einfra will be created
  + resource "openstack_identity_provider" "login_einfra" {
      + domain_id  = (known after apply)
      + enabled    = true
      + id         = (known after apply)
      + remote_ids = [
          + "https://login.e-infra.cz/oidc/",
        ]
    }

  # openstack_mapping.mapping_1 will be created
  + resource "openstack_mapping" "mapping_1" {
      + id         = (known after apply)
      + mapping_id = "mapping-test-id"

      + rules {
          + local {
              + group {
                  + name = "e_infra_cz_federated_users"

                  + domain {
                      + id = "0b2320313906481688277bbe99304732"
                    }
                }
            }

          + remote {
              + type = "OIDC-group"
            }
        }
    }

Plan: 4 to add, 0 to change, 0 to destroy.

```
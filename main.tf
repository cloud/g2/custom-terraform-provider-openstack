terraform {
  required_providers {
    openstack = {
      version = "~> 0.0.1"
      source  = "gitlab.ics.muni.cz/cloud/openstack"
    }
  }
}

provider "openstack" {
  url = "http://os.local"
  port = 80
  auth_url = "http://os.local/v3"
  auth_url_port = 80

  // v3applicationcredential || password
  auth_type = "v3applicationcredential"

  // password
#  username = "admin"
#  password = "admin"
#  project_name = "testing-project"
#  default_domain_name = "Default"

  // v3applicationcredential
  client_id = "admin"
  client_secret = "admin"
}

resource "openstack_domain" "einfra_cz" {
#  explicit_id = "einfra_cz"
  name = "einfra_cz"
  description = "e-INFRA.cz domain"
}

#resource "openstack_identity_provider" "login_einfra" {
#  identity_provider_id = "login_einfra"
#  domain_id = openstack_domain.einfra_cz.id
#  remote_ids = ["https://login.e-infra.cz/oidc/"]
#}
#
#resource "openstack_mapping" "mapping_1" {
#
#  mapping_id = "mapping_1"
#
#  rules {
#    local {
#      group {
#        name = "e_infra_cz_federated_users"
#        domain {
#          id = "0b2320313906481688277bbe99304732"
#        }
#      }
#    }
#
#    remote {
#      type = "OIDC-group"
#    }
#  }
#}
#
#resource "openstack_federation_protocol" "openid" {
#  protocol_id = "openid"
#  identity_provider_id = openstack_identity_provider.login_einfra.id
#  mapping_id = "mapping_1"
#}
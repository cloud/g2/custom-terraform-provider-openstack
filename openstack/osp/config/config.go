package config

import (
	"fmt"
	"log"
	"main/openstack/osc"
	"net/url"
)

type Config struct {
	// auth
	AuthUrl     string
	AuthUrlPort int
	AuthType    string // v3applicationcredential || password

	// auth_type=password
	Username          string
	Password          string
	ProjectName       string
	DefaultDomainName string

	// auth_type=v3applicationcredential
	ClientID     string
	ClientSecret string

	Url  string
	Port int

	Token string

	TerraformVersion string
}

type CombinedConfig struct {
	Client *osc.Client
}

func (c *CombinedConfig) OssClient() *osc.Client {
	return c.Client
}

func (c *Config) Client() (*CombinedConfig, error) {
	userAgent := fmt.Sprintf("Terraform/%s", c.TerraformVersion)
	//client := oauth2.NewClient(oauth2.NoContext, tokenSrc)

	//client.Transport = logging.NewTransport("openstack", client.Transport)

	oscClient, err := osc.New(nil, osc.SetUserAgent(userAgent))
	if err != nil {
		return nil, err
	}

	apiURL, err := url.Parse(c.Url)
	if err != nil {
		return nil, err
	}

	authUrl, err := url.Parse(c.AuthUrl)
	if err != nil {
		return nil, err
	}

	oscClient.FullAuthURL = authUrl

	// obtain auth token
	createTokenRequest := &osc.TokenCreateRequest{}

	if c.AuthType == "v3applicationcredential" {
		createTokenRequest = &osc.TokenCreateRequest{
			Auth: osc.TokenCreateRequestPart{
				Identity: osc.TokenCreateRequestIdentity{
					Methods: []string{"application_credential"},
					ApplicationCredential: &osc.ApplicationCredential{
						ID:     c.ClientID,
						Secret: c.ClientSecret,
					},
				},
			},
		}
	}

	if c.AuthType == "password" {
		createTokenRequest = &osc.TokenCreateRequest{
			Auth: osc.TokenCreateRequestPart{
				Identity: osc.TokenCreateRequestIdentity{
					Methods: []string{"password"},
					Password: &osc.Password{
						User: &osc.AuthUser{
							Domain: &osc.AuthDomain{
								Name: c.DefaultDomainName,
							},
							Name:     c.Username,
							Password: c.Password,
						},
					},
				},
			},
		}
	}

	token, _, err := oscClient.AuthService.Create(createTokenRequest)
	if err != nil {
		return nil, err
	}

	oscClient.SetAuthToken(token.TokenID)
	oscClient.BaseURL = apiURL

	log.Printf("[INFO] openstack Client configured for URL: %s", oscClient.BaseURL.String())

	return &CombinedConfig{
		Client: oscClient,
	}, nil
}

package osp

import (
	"github.com/hashicorp/terraform-plugin-sdk/helper/schema"
	"github.com/hashicorp/terraform-plugin-sdk/terraform"
	"main/openstack/osp/config"
	"testing"
)

func TestProvider(t *testing.T) {
	if err := Provider().InternalValidate(); err != nil {
		t.Fatalf("err: %s", err)
	}
}

func TestProvider_impl(t *testing.T) {
	var _ *schema.Provider = Provider()
}

func TestURLOverride(t *testing.T) {
	customEndpoint := "http://openstack"

	rawProvider := Provider()
	raw := map[string]interface{}{
		"token":        "12345",
		"api_endpoint": customEndpoint,
	}

	diags := rawProvider.Configure(terraform.NewResourceConfigRaw(raw))
	if diags != nil && diags.Error() != "" {
		t.Fatalf("provider configure failed: %s", diags.Error())
	}

	meta := rawProvider.Meta()
	if meta == nil {
		t.Fatalf("Expected metadata, got nil")
	}

	client := meta.(*config.CombinedConfig).OssClient()
	if client.BaseURL.String() != customEndpoint {
		t.Fatalf("Expected %s, got %s", customEndpoint, client.BaseURL.String())
	}
}

func TestURLDefault(t *testing.T) {
	rawProvider := Provider()
	raw := map[string]interface{}{
		"token": "12345",
	}

	diags := rawProvider.Configure(terraform.NewResourceConfigRaw(raw))
	if diags != nil && diags.Error() != "" {
		t.Fatalf("provider configure failed: %s", diags.Error())
	}

	meta := rawProvider.Meta()
	if meta == nil {
		t.Fatal("Expected metadata, got nil")
	}

	expectedURL := "http://openstack"

	client := meta.(*config.CombinedConfig).OssClient()
	if client.BaseURL.String() != expectedURL {
		t.Fatalf("Expected %s, got %s", expectedURL, client.BaseURL.String())
	}
}

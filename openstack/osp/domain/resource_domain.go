package domain

import (
	"fmt"
	"github.com/hashicorp/terraform-plugin-sdk/helper/schema"
	"log"
	"main/openstack/osc"
	"main/openstack/osp/config"
)

func ResourceOpenStackDomain() *schema.Resource {
	return &schema.Resource{
		Create: resourceOpenStackDomainCreate,
		Read:   resourceOpenStackDomainRead,
		Delete: resourceOpenStackDomainDelete,
		Update: resourceOpenStackDomainUpdate,
		//Importer: &schema.ResourceImporter{
		//	State: schema.ImportStatePassthrough,
		//},
		Schema: map[string]*schema.Schema{
			"name": {
				Type:     schema.TypeString,
				Required: true,
			},
			"enabled": {
				Type:     schema.TypeBool,
				Optional: true,
				Default:  true,
			},
			"description": {
				Type:     schema.TypeString,
				Optional: true,
			},
			"explicit_id": {
				Type:     schema.TypeString,
				Optional: true,
			},
		},
	}
}

func resourceOpenStackDomainCreate(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*config.CombinedConfig).OssClient()

	// Build up our creation options

	opts := &osc.DomainCreateRequest{}

	if v, ok := d.GetOk("name"); ok {
		opts.Domain.Name = v.(string)
	}

	if v, ok := d.GetOk("description"); ok {
		opts.Domain.Description = v.(string)
	}

	if v, ok := d.GetOk("enabled"); ok {
		opts.Domain.Enabled = v.(bool)
	}

	if v, ok := d.GetOk("explicit_id"); ok {
		opts.Domain.ExplicitDomainID = v.(string)
	}

	log.Printf("[DEBUG] Domain create configuration: %#v", opts)
	domain, _, err := client.Domains.Create(opts)
	if err != nil {
		return fmt.Errorf("error creating Domain: %s", err)
	}

	d.SetId(domain.ID)

	log.Printf("[INFO] Domain ID: %s", domain.ID)

	return resourceOpenStackDomainRead(d, meta)
}

func resourceOpenStackDomainUpdate(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*config.CombinedConfig).OssClient()

	// Build up our update options

	opts := &osc.DomainUpdateRequest{
		Domain: osc.DomainUpdateRequestPart{
			ID: d.Id(),
		},
	}

	if v, ok := d.GetOk("name"); ok {
		opts.Domain.Name = v.(string)
	}

	if v, ok := d.GetOk("description"); ok {
		opts.Domain.Description = v.(string)
	}

	if v, ok := d.GetOk("enabled"); ok {
		opts.Domain.Enabled = v.(bool)
	}

	log.Printf("[DEBUG] Domain update configuration: %#v", opts)
	domain, _, err := client.Domains.Update(opts)
	if err != nil {
		return fmt.Errorf("error creating Domain: %s", err)
	}

	log.Printf("[INFO] Domain ID: %s", domain.ID)

	return resourceOpenStackDomainRead(d, meta)
}

func resourceOpenStackDomainRead(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*config.CombinedConfig).OssClient()

	domain, resp, err := client.Domains.Get(d.Id())
	if err != nil {
		// If the domain is somehow already destroyed, mark as
		// successfully gone
		if resp != nil && resp.StatusCode == 404 {
			d.SetId("")
			return nil
		}

		return fmt.Errorf("error retrieving domain: %s", err)
	}

	d.Set("name", domain.Name)
	d.Set("description", domain.Description)
	d.Set("enabled", domain.Enabled)

	return nil
}

func resourceOpenStackDomainDelete(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*config.CombinedConfig).OssClient()

	log.Printf("[INFO] Deleting Domain: %s", d.Id())
	_, err := client.Domains.Delete(d.Id())
	if err != nil {
		return fmt.Errorf("error deleting Domain: %s", err)
	}

	d.SetId("")
	d.Set("name", "")
	d.Set("description", "")
	return nil
}

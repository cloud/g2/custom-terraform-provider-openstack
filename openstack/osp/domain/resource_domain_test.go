package domain_test

import (
	"fmt"
	"github.com/hashicorp/terraform-plugin-sdk/helper/resource"
	"github.com/hashicorp/terraform-plugin-sdk/helper/schema"
	"github.com/stretchr/testify/assert"
	"main/openstack/osc"
	"main/openstack/osp/acceptance"
	"main/openstack/osp/config"
	"testing"
)

//

//

func TestSimpleCreate(t *testing.T) {
	expectedID := "123"
	expectedName := "test.com"
	expectedDescription := "test description"
	expectedEnabled := "true"

	acceptance.ResetFakeService()

	acceptance.TestAccProvider.ConfigureFunc = func(data *schema.ResourceData) (interface{}, error) {
		return &config.CombinedConfig{
			Client: &osc.Client{
				Domains: &acceptance.DomainsServiceFake{},
			},
		}, nil
	}

	acceptance.FakeCreateFn = func(createRequest *osc.DomainCreateRequest) (*osc.Domain, *osc.Response, error) {

		if createRequest.Domain.ID != "" {
			return nil, nil, fmt.Errorf("domain id cannot be specified: %s", createRequest.Domain.ID)
		}

		if createRequest.Domain.Description == "test.com" {
			return nil, nil, fmt.Errorf("domain name is not correct: %s", createRequest.Domain.Name)
		}

		return &osc.Domain{
			ID:          "123",
			Name:        "test.com",
			Description: "test description",
			Enabled:     true,
		}, nil, nil
	}

	acceptance.FakeGetFn = func(id string) (*osc.Domain, *osc.Response, error) {
		return &osc.Domain{
			ID:          "123",
			Name:        "test.com",
			Description: "test description",
			Enabled:     true,
		}, nil, nil
	}

	resource.Test(t, resource.TestCase{
		PreCheck:          func() { acceptance.TestAccPreCheck(t) },
		ProviderFactories: acceptance.TestAccProviderFactories,
		Steps: []resource.TestStep{
			{
				Config: fmt.Sprintf(test_create_domain, expectedName),
				Check: resource.ComposeTestCheckFunc(
					resource.TestCheckResourceAttr(
						"openstack_domain.foobar", "id", expectedID),
					resource.TestCheckResourceAttr(
						"openstack_domain.foobar", "name", expectedName),
					resource.TestCheckResourceAttr(
						"openstack_domain.foobar", "description", expectedDescription),
					resource.TestCheckResourceAttr(
						"openstack_domain.foobar", "enabled", expectedEnabled),
				),
			},
		},
	})
}

func TestCreateWithExplicitID(t *testing.T) {
	expectedName := "test.com"
	expectedDescription := "test description"
	expectedEnabled := "true"
	expectedExplicitID := "explicit-id-example"

	acceptance.TestAccProvider.ConfigureFunc = func(data *schema.ResourceData) (interface{}, error) {
		return &config.CombinedConfig{
			Client: &osc.Client{
				Domains: &acceptance.DomainsServiceFake{},
			},
		}, nil
	}

	acceptance.ResetFakeService()

	acceptance.FakeCreateFn = func(createRequest *osc.DomainCreateRequest) (*osc.Domain, *osc.Response, error) {

		assert.Equal(t, createRequest.Domain.ExplicitDomainID, expectedExplicitID)

		return &osc.Domain{
			ID:          expectedExplicitID,
			Name:        "test.com",
			Description: "test description",
			Enabled:     true,
		}, nil, nil
	}

	acceptance.FakeGetFn = func(id string) (*osc.Domain, *osc.Response, error) {
		return &osc.Domain{
			ID:          expectedExplicitID,
			Name:        "test.com",
			Description: "test description",
			Enabled:     true,
		}, nil, nil
	}

	resource.Test(t, resource.TestCase{
		PreCheck:          func() { acceptance.TestAccPreCheck(t) },
		ProviderFactories: acceptance.TestAccProviderFactories,
		Steps: []resource.TestStep{
			{
				Config: fmt.Sprintf(testDomainWithExplicitID, expectedName),
				Check: resource.ComposeTestCheckFunc(
					resource.TestCheckResourceAttr(
						"openstack_domain.foobar", "id", expectedExplicitID),
					resource.TestCheckResourceAttr(
						"openstack_domain.foobar", "name", expectedName),
					resource.TestCheckResourceAttr(
						"openstack_domain.foobar", "description", expectedDescription),
					resource.TestCheckResourceAttr(
						"openstack_domain.foobar", "enabled", expectedEnabled),
				),
			},
		},
	})
}

func Test_create_with_disabled(t *testing.T) {
	expectedName := "test.com"
	expectedDescription := "test description"
	expectedEnabled := "false"

	acceptance.TestAccProvider.ConfigureFunc = func(data *schema.ResourceData) (interface{}, error) {
		return &config.CombinedConfig{
			Client: &osc.Client{
				Domains: &acceptance.DomainsServiceFake{},
			},
		}, nil
	}

	acceptance.ResetFakeService()

	acceptance.FakeCreateFn = func(createRequest *osc.DomainCreateRequest) (*osc.Domain, *osc.Response, error) {

		return &osc.Domain{
			ID:          "123",
			Name:        "test.com",
			Description: "test description",
			Enabled:     false,
		}, nil, nil
	}

	acceptance.FakeGetFn = func(id string) (*osc.Domain, *osc.Response, error) {
		return &osc.Domain{
			ID:          "123",
			Name:        "test.com",
			Description: "test description",
			Enabled:     false,
		}, nil, nil
	}

	resource.Test(t, resource.TestCase{
		PreCheck:          func() { acceptance.TestAccPreCheck(t) },
		ProviderFactories: acceptance.TestAccProviderFactories,
		Steps: []resource.TestStep{
			{
				Config: fmt.Sprintf(testDomainDisabled, expectedName),
				Check: resource.ComposeTestCheckFunc(
					resource.TestCheckResourceAttr(
						"openstack_domain.foobar", "id", "123"),
					resource.TestCheckResourceAttr(
						"openstack_domain.foobar", "name", expectedName),
					resource.TestCheckResourceAttr(
						"openstack_domain.foobar", "description", expectedDescription),
					resource.TestCheckResourceAttr(
						"openstack_domain.foobar", "enabled", expectedEnabled),
				),
			},
		},
	})
}

//func Test_domain_update(t *testing.T) {
//
//	acceptance.TestAccProvider.ConfigureFunc = func(data *schema.ResourceData) (interface{}, error) {
//		return &config.CombinedConfig{
//			Client: &osc.Client{
//				Domains: &acceptance.DomainsServiceFake{},
//			},
//		}, nil
//	}
//
//	acceptance.ResetFakeService()
//
//	acceptance.FakeGetFn = func(id string) (*osc.Domain, *osc.Response, error) {
//		return &osc.Domain{
//			ID:          "123",
//			Name:        "test.com",
//			Description: "old description",
//			Enabled:     true,
//		}, nil, nil
//	}
//
//	acceptance.FakeUpdateFn = func(updateRequest *osc.DomainUpdateRequest) (*osc.Domain, *osc.Response, error) {
//
//		if updateRequest.Domain.Description != "new description" {
//			t.Fatalf("expected description to be 'new description', got '%s'", updateRequest.Domain.Description)
//		}
//
//		return &osc.Domain{
//			ID:          "123",
//			Name:        "test.com",
//			Description: "new description",
//			Enabled:     true,
//		}, nil, nil
//
//	}
//
//	resource.Test(t, resource.TestCase{
//		PreCheck:          func() { acceptance.TestAccPreCheck(t) },
//		ProviderFactories: acceptance.TestAccProviderFactories,
//		Steps: []resource.TestStep{
//			{
//				Config: test_domain_update,
//
//				Check: resource.ComposeTestCheckFunc(
//					resource.TestCheckResourceAttr(
//						"openstack_domain.foobar", "id", "123"),
//					resource.TestCheckResourceAttr(
//						"openstack_domain.foobar", "name", "test.com"),
//					resource.TestCheckResourceAttr(
//						"openstack_domain.foobar", "description", "new description"),
//					resource.TestCheckResourceAttr(
//						"openstack_domain.foobar", "enabled", "true"),
//				),
//			},
//		},
//	})
//}

const test_create_domain = `
resource "openstack_domain" "foobar" {
 name       = "%s"
 description = "test description"
 enabled = true
}`

const testDomainWithExplicitID = `
resource "openstack_domain" "foobar" {
 explicit_id = "explicit-id-example"
 name       = "%s"
 description = "test description"
 enabled = true
}`

const testDomainDisabled = `
resource "openstack_domain" "foobar" {
 name       = "%s"
 description = "test description"
 enabled = false
}`

//const test_domain_update = `
//resource "openstack_domain" "foobar" {
// name       = "test.com"
// description = "new description"
// enabled = true
//}`

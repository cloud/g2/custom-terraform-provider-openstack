package federation_protocol_test

import (
	"github.com/hashicorp/terraform-plugin-sdk/helper/resource"
	"github.com/hashicorp/terraform-plugin-sdk/helper/schema"
	"main/openstack/osc"
	"main/openstack/osp/acceptance"
	"main/openstack/osp/config"
	"testing"
)

//

//

func TestSimpleCreate(t *testing.T) {

	acceptance.TestAccProvider.ConfigureFunc = func(data *schema.ResourceData) (interface{}, error) {
		return &config.CombinedConfig{
			Client: &osc.Client{
				FederationProtocols: &acceptance.FederationProtocolsServiceFake{},
			},
		}, nil
	}

	acceptance.FakeCreateFPFn = func(createRequest *osc.FederationProtocolCreateRequest) (*osc.FederationProtocol, *osc.Response, error) {
		return &osc.FederationProtocol{
			ID:               "openid",
			IdentityProvider: "login.e-infra.cz",
			Mapping:          "login_e_infra_cz_mapping",
		}, nil, nil
	}

	acceptance.FakeGetFPFn = func(idp_id string, id string) (*osc.FederationProtocol, *osc.Response, error) {
		return &osc.FederationProtocol{
			ID:               "openid",
			IdentityProvider: "login.e-infra.cz",
			Mapping:          "login_e_infra_cz_mapping",
		}, nil, nil
	}

	resource.Test(t, resource.TestCase{
		PreCheck:          func() { acceptance.TestAccPreCheck(t) },
		ProviderFactories: acceptance.TestAccProviderFactories,
		Steps: []resource.TestStep{
			{
				Config: test_fp_default,

				Check: resource.ComposeTestCheckFunc(
					resource.TestCheckResourceAttr("openstack_federation_protocol.openid", "id", "openid"),
					resource.TestCheckResourceAttr("openstack_federation_protocol.openid", "protocol_id", "openid"),
					resource.TestCheckResourceAttr("openstack_federation_protocol.openid", "identity_provider_id", "login.e-infra.cz"),
					resource.TestCheckResourceAttr("openstack_federation_protocol.openid", "mapping_id", "login_e_infra_cz_mapping"),
				),
			},
		},
	})
}

//func TestCreateWithExplicitID(t *testing.T) {
//	expectedName := "test.com"
//	expectedDescription := "test description"
//	expectedEnabled := "true"
//	expectedExplicitID := "explicit-id-example"
//
//	acceptance.TestAccProvider.ConfigureFunc = func(data *schema.ResourceData) (interface{}, error) {
//		return &config.CombinedConfig{
//			Client: &osc.Client{
//				Domains: &acceptance.DomainsServiceFake{},
//			},
//		}, nil
//	}
//
//	acceptance.FakeCreateFn = func(createRequest *osc.DomainCreateRequest) (*osc.Domain, *osc.Response, error) {
//
//		assert.Equal(t, createRequest.Domain.ExplicitDomainID, expectedExplicitID)
//
//		return &osc.Domain{
//			ID:          expectedExplicitID,
//			Name:        "test.com",
//			Description: "test description",
//			Enabled:     true,
//		}, nil, nil
//	}
//
//	acceptance.FakeGetFn = func(id string) (*osc.Domain, *osc.Response, error) {
//		return &osc.Domain{
//			ID:          expectedExplicitID,
//			Name:        "test.com",
//			Description: "test description",
//			Enabled:     true,
//		}, nil, nil
//	}
//
//	resource.Test(t, resource.TestCase{
//		PreCheck:          func() { acceptance.TestAccPreCheck(t) },
//		ProviderFactories: acceptance.TestAccProviderFactories,
//		Steps: []resource.TestStep{
//			{
//				Config: fmt.Sprintf(testDomainWithExplicitID, expectedName),
//				Check: resource.ComposeTestCheckFunc(
//					resource.TestCheckResourceAttr(
//						"openstack_identity_provider.acme", "id", expectedExplicitID),
//					resource.TestCheckResourceAttr(
//						"openstack_identity_provider.acme", "name", expectedName),
//					resource.TestCheckResourceAttr(
//						"openstack_identity_provider.acme", "description", expectedDescription),
//					resource.TestCheckResourceAttr(
//						"openstack_identity_provider.acme", "enabled", expectedEnabled),
//				),
//			},
//		},
//	})
//}
//
//func Test_create_with_disabled(t *testing.T) {
//	expectedName := "test.com"
//	expectedDescription := "test description"
//	expectedEnabled := "false"
//
//	acceptance.TestAccProvider.ConfigureFunc = func(data *schema.ResourceData) (interface{}, error) {
//		return &config.CombinedConfig{
//			Client: &osc.Client{
//				Domains: &acceptance.DomainsServiceFake{},
//			},
//		}, nil
//	}
//
//	acceptance.FakeCreateFn = func(createRequest *osc.DomainCreateRequest) (*osc.Domain, *osc.Response, error) {
//
//		return &osc.Domain{
//			ID:          "123",
//			Name:        "test.com",
//			Description: "test description",
//			Enabled:     false,
//		}, nil, nil
//	}
//
//	acceptance.FakeGetFn = func(id string) (*osc.Domain, *osc.Response, error) {
//		return &osc.Domain{
//			ID:          "123",
//			Name:        "test.com",
//			Description: "test description",
//			Enabled:     false,
//		}, nil, nil
//	}
//
//	resource.Test(t, resource.TestCase{
//		PreCheck:          func() { acceptance.TestAccPreCheck(t) },
//		ProviderFactories: acceptance.TestAccProviderFactories,
//		Steps: []resource.TestStep{
//			{
//				Config: fmt.Sprintf(testDomainDisabled, expectedName),
//				Check: resource.ComposeTestCheckFunc(
//					resource.TestCheckResourceAttr(
//						"openstack_identity_provider.acme", "id", "123"),
//					resource.TestCheckResourceAttr(
//						"openstack_identity_provider.acme", "name", expectedName),
//					resource.TestCheckResourceAttr(
//						"openstack_identity_provider.acme", "description", expectedDescription),
//					resource.TestCheckResourceAttr(
//						"openstack_identity_provider.acme", "enabled", expectedEnabled),
//				),
//			},
//		},
//	})
//}

const test_fp_default = `
resource "openstack_federation_protocol" "openid" {
	protocol_id = "openid"
	identity_provider_id = "login.e-infra.cz"
	mapping_id = "login_e_infra_cz_mapping"
}
`

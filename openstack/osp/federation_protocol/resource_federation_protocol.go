package federation_protocol

import (
	"fmt"
	"github.com/hashicorp/terraform-plugin-sdk/helper/schema"
	"log"
	"main/openstack/osc"
	"main/openstack/osp/config"
)

func ResourceOpenStackFederationProvider() *schema.Resource {
	return &schema.Resource{
		Create: resourceOpenStackFederationProtocolCreate,
		Read:   resourceOpenStackFederationProtocolRead,
		Delete: resourceOpenStackFederationProtocolDelete,
		Update: resourceOpenStackFederationProtocolUpdate,
		//Importer: &schema.ResourceImporter{
		//	State: schema.ImportStatePassthrough,
		//},
		Schema: map[string]*schema.Schema{
			"protocol_id": {
				Type:     schema.TypeString,
				Required: true,
			},
			"identity_provider_id": {
				Type:     schema.TypeString,
				Required: true,
			},
			"mapping_id": {
				Type:     schema.TypeString,
				Required: true,
			},
			"remote_id_attribute": {
				Type:     schema.TypeString,
				Optional: true,
			},
		},
	}
}

func resourceOpenStackFederationProtocolCreate(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*config.CombinedConfig).OssClient()
	opts := &osc.FederationProtocolCreateRequest{}

	// required
	opts.FederationProtocol.ID = d.Get("protocol_id").(string)
	opts.FederationProtocol.IdentityProvider = d.Get("identity_provider_id").(string)
	opts.FederationProtocol.Mapping = d.Get("mapping_id").(string)

	// optional
	if v, ok := d.GetOk("remote_id_attribute"); ok {
		opts.FederationProtocol.RemoteIDAttribute = v.(string)
	}

	log.Printf("[DEBUG] FederationProtocol create configuration: %#v", opts)
	ip, _, err := client.FederationProtocols.Create(opts)

	if err != nil {
		return fmt.Errorf("error creating federatino protocol: %s", err)
	}

	d.SetId(ip.ID)

	log.Printf("[INFO] FederationProtocol ID: %s", ip.ID)

	return resourceOpenStackFederationProtocolRead(d, meta)
}

func resourceOpenStackFederationProtocolUpdate(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*config.CombinedConfig).OssClient()

	opts := &osc.FederationProtocolUpdateRequest{
		FederationProtocol: osc.FederationProtocolUpdateRequestPart{
			IdentityProvider: d.Get("identity_provider_id").(string),
			ID:               d.Id(),
			Mapping:          d.Get("mapping_id").(string),
		},
	}

	if v, ok := d.GetOk("remote_id_attribute"); ok {
		opts.FederationProtocol.RemoteIDAttribute = v.(string)
	}

	log.Printf("[DEBUG] FederationProtocol update configuration: %#v", opts)
	ip, _, err := client.FederationProtocols.Update(opts)

	if err != nil {
		return fmt.Errorf("error creating FederationProtocol: %s", err)
	}

	log.Printf("[INFO] FederationProtocol ID: %s", ip.ID)

	return resourceOpenStackFederationProtocolRead(d, meta)
}

func resourceOpenStackFederationProtocolRead(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*config.CombinedConfig).OssClient()

	identityProviderID := d.Get("identity_provider_id").(string)

	fp, resp, err := client.FederationProtocols.Get(identityProviderID, d.Id())
	if err != nil {
		if resp != nil && resp.StatusCode == 404 {
			d.SetId("")
			return nil
		}

		return fmt.Errorf("error retrieving identity provider: %s", err)
	}

	d.Set("protocol_id", fp.ID)
	d.Set("identity_provider_id", fp.IdentityProvider)
	d.Set("mapping_id", fp.Mapping)
	d.Set("remote_id_attribute", fp.RemoteIDAttribute)

	return nil
}

func resourceOpenStackFederationProtocolDelete(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*config.CombinedConfig).OssClient()

	identityProviderID := d.Get("identity_provider_id").(string)

	log.Printf("[INFO] identity provider: %s", d.Id())
	_, err := client.FederationProtocols.Delete(identityProviderID, d.Id())
	if err != nil {
		return fmt.Errorf("error deleting identity provider: %s", err)
	}

	d.SetId("")

	return nil
}

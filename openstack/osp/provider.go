package osp

import (
	"github.com/hashicorp/terraform-plugin-sdk/helper/schema"
	"main/openstack/osp/config"
	"main/openstack/osp/domain"
	"main/openstack/osp/federation_protocol"
	"main/openstack/osp/identity_provider"
	"main/openstack/osp/mapping"
)

func Provider() *schema.Provider {
	p := &schema.Provider{
		Schema: map[string]*schema.Schema{

			"url": {
				Type:        schema.TypeString,
				Required:    true,
				DefaultFunc: schema.EnvDefaultFunc("SERVICE_URL", "http://openstack"),
				Description: "The URL to use for the openstack API.",
			},
			"port": {
				Type:        schema.TypeInt,
				Optional:    true,
				DefaultFunc: schema.EnvDefaultFunc("SERVICE_PORT", 80),
				Description: "The URL to use for the openstack Spaces API.",
			},

			// v3applicationcredential || password
			"auth_type": {
				Type:        schema.TypeString,
				Optional:    true,
				DefaultFunc: schema.EnvDefaultFunc("OS_AUTH_TYPE", "password"),
				Description: "The authentication method. For password authentication, specify password.",
			},
			"auth_url": {
				Type:        schema.TypeString,
				Required:    true,
				DefaultFunc: schema.EnvDefaultFunc("OS_AUTH_URL", "https://identity.stage.cloud.muni.cz/v3"),
			},
			"auth_url_port": {
				Type:        schema.TypeInt,
				Required:    true,
				DefaultFunc: schema.EnvDefaultFunc("OS_AUTH_URL_PORT", 80),
			},

			// v3applicationcredential
			"client_id": {
				Type:         schema.TypeString,
				Optional:     true,
				RequiredWith: []string{"client_secret"},
				DefaultFunc:  schema.EnvDefaultFunc("OS_APPLICATION_CREDENTIAL_ID", 80),
			},
			"client_secret": {
				Type:         schema.TypeString,
				Optional:     true,
				RequiredWith: []string{"client_id"},
				DefaultFunc:  schema.EnvDefaultFunc("OS_APPLICATION_CREDENTIAL_SECRET", 80),
			},

			// password
			"username": {
				Type:         schema.TypeString,
				Optional:     true,
				RequiredWith: []string{"password", "project_name", "default_domain_name"},
				DefaultFunc:  schema.EnvDefaultFunc("OS_USERNAME", ""),
			},
			"password": {
				Type:         schema.TypeString,
				Optional:     true,
				RequiredWith: []string{"username", "project_name", "default_domain_name"},
				DefaultFunc:  schema.EnvDefaultFunc("OS_PASSWORD", ""),
			},
			"project_name": {
				Type:         schema.TypeString,
				Optional:     true,
				RequiredWith: []string{"username", "password", "default_domain_name"},
				DefaultFunc:  schema.EnvDefaultFunc("OS_PROJECT_NAME", ""),
			},
			"default_domain_name": {
				Type:         schema.TypeString,
				Optional:     true,
				RequiredWith: []string{"username", "password", "project_name"},
				DefaultFunc:  schema.EnvDefaultFunc("OS_DEFAULT_DOMAIN_NAME", ""),
			},
		},

		ResourcesMap: map[string]*schema.Resource{
			"openstack_domain":              domain.ResourceOpenStackDomain(),
			"openstack_identity_provider":   identity_provider.ResourceOpenStackIdentityProvider(),
			"openstack_mapping":             mapping.ResourceOpenStackMapping(),
			"openstack_federation_protocol": federation_protocol.ResourceOpenStackFederationProvider(),
		},
	}

	p.ConfigureFunc = func(d *schema.ResourceData) (interface{}, error) {
		terraformVersion := p.TerraformVersion
		if terraformVersion == "" {
			// Terraform 0.12 introduced this field to the protocol
			// We can therefore assume that if it's missing it's 0.10 or 0.11
			terraformVersion = "0.11+compatible"
		}
		return providerConfigure(d, terraformVersion)
	}

	return p
}

func providerConfigure(d *schema.ResourceData, terraformVersion string) (interface{}, error) {
	conf := config.Config{
		Url:  d.Get("url").(string),
		Port: d.Get("port").(int),

		AuthUrl:     d.Get("auth_url").(string),
		AuthUrlPort: d.Get("auth_url_port").(int),

		// v3applicationcredential || password
		AuthType: d.Get("auth_type").(string),

		// v3applicationcredential
		ClientID:     d.Get("client_id").(string),
		ClientSecret: d.Get("client_secret").(string),

		// password
		Username:          d.Get("username").(string),
		Password:          d.Get("password").(string),
		ProjectName:       d.Get("project_name").(string),
		DefaultDomainName: d.Get("default_domain_name").(string),

		TerraformVersion: terraformVersion,
	}

	return conf.Client()
}

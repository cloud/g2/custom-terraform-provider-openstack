package acceptance

import (
	"github.com/hashicorp/terraform-plugin-sdk/helper/schema"
	"github.com/hashicorp/terraform-plugin-sdk/terraform"
	"main/openstack/osp"
	"testing"
)

const TestNamePrefix = "tf-acc-test-"

var (
	TestAccProvider          *schema.Provider
	TestAccProviders         map[string]*schema.Provider
	TestAccProviderFactories map[string]terraform.ResourceProviderFactory
)

func init() {
	TestAccProvider = osp.Provider()
	TestAccProviders = map[string]*schema.Provider{
		"openstack": TestAccProvider,
	}
	TestAccProviderFactories = map[string]terraform.ResourceProviderFactory{
		"openstack": func() (terraform.ResourceProvider, error) {
			return TestAccProvider, nil
		},
	}
}

func TestAccPreCheck(t *testing.T) {
	//if v := os.Getenv("OPENSTACK_TOKEN"); v == "" {
	//	t.Fatal("OPENSTACK_TOKEN must be set for acceptance tests")
	//}

	err := TestAccProvider.Configure(terraform.NewResourceConfigRaw(nil))

	if err != nil {
		t.Fatal(err)
	}
}

package acceptance

import "main/openstack/osc"

type MappingsServiceFake struct {
}

var _ osc.MappingsService = &MappingsServiceFake{}

var fakeCreateMappingFnDefault = func(createRequest *osc.MappingCreateRequest) (*osc.Mapping, *osc.Response, error) {
	return nil, nil, nil
}

var FakeCreateMappingFn = fakeCreateMappingFnDefault

var fakeListMappingFnDefault = func(opt *osc.ListOptions) ([]osc.Mapping, *osc.Response, error) {
	return nil, nil, nil
}
var FakeListMappingFn = fakeListMappingFnDefault

var fakeGetMappingFnDefault = func(id string) (*osc.Mapping, *osc.Response, error) {
	return nil, nil, nil
}

var FakeGetMappingFn = fakeGetMappingFnDefault

var fakeUpdateMappingFnDefault = func(updateRequest *osc.MappingUpdateRequest) (*osc.Mapping, *osc.Response, error) {
	return nil, nil, nil
}
var FakeUpdateMappingFn = fakeUpdateMappingFnDefault

var fakeDeleteMappingFnDefault = func(name string) (*osc.Response, error) {
	return nil, nil
}
var FakeDeleteMappingFn = fakeDeleteMappingFnDefault

func ResetFakeMapping() {
	FakeCreateMappingFn = fakeCreateMappingFnDefault
	FakeListMappingFn = fakeListMappingFnDefault
	FakeGetMappingFn = fakeGetMappingFnDefault
	FakeUpdateMappingFn = fakeUpdateMappingFnDefault
	FakeDeleteMappingFn = fakeDeleteMappingFnDefault
}

func (s MappingsServiceFake) List(opt *osc.ListOptions) ([]osc.Mapping, *osc.Response, error) {
	return FakeListMappingFn(opt)
}

func (s MappingsServiceFake) Get(id string) (*osc.Mapping, *osc.Response, error) {
	return FakeGetMappingFn(id)
}

func (s *MappingsServiceFake) Create(createRequest *osc.MappingCreateRequest) (*osc.Mapping, *osc.Response, error) {
	return FakeCreateMappingFn(createRequest)
}

func (s *MappingsServiceFake) Update(updateRequest *osc.MappingUpdateRequest) (*osc.Mapping, *osc.Response, error) {
	return FakeUpdateMappingFn(updateRequest)
}

func (s *MappingsServiceFake) Delete(name string) (*osc.Response, error) {
	return FakeDeleteMappingFn(name)
}

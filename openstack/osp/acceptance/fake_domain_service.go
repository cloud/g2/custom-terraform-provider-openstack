package acceptance

import (
	"fmt"
	"main/openstack/osc"
)

type DomainsServiceFake struct {
}

var _ osc.DomainsService = &DomainsServiceFake{}

var fakeCreateFnDefault = func(createRequest *osc.DomainCreateRequest) (*osc.Domain, *osc.Response, error) {
	return nil, nil, fmt.Errorf("fakeCreateFnDefault not implemented")
}

var FakeCreateFn = fakeCreateFnDefault

var fakeListFnDefault = func(opt *osc.ListOptions) ([]osc.Domain, *osc.Response, error) {
	return nil, nil, nil
}
var FakeListFn = fakeListFnDefault

var fakeGetFnDefault = func(id string) (*osc.Domain, *osc.Response, error) {
	return nil, nil, nil
}

var FakeGetFn = fakeGetFnDefault

var fakeUpdateFnDefault = func(updateRequest *osc.DomainUpdateRequest) (*osc.Domain, *osc.Response, error) {
	return nil, nil, nil
}
var FakeUpdateFn = fakeUpdateFnDefault

var fakeDeleteFnDefault = func(name string) (*osc.Response, error) {
	return nil, nil
}
var FakeDeleteFn = fakeDeleteFnDefault

func ResetFakeService() {
	FakeCreateFn = fakeCreateFnDefault
	FakeListFn = fakeListFnDefault
	FakeGetFn = fakeGetFnDefault
	FakeUpdateFn = fakeUpdateFnDefault
	FakeDeleteFn = fakeDeleteFnDefault
}

func (s DomainsServiceFake) List(opt *osc.ListOptions) ([]osc.Domain, *osc.Response, error) {
	return FakeListFn(opt)
}

// Get returns a domain
func (s DomainsServiceFake) Get(id string) (*osc.Domain, *osc.Response, error) {
	return FakeGetFn(id)
}

// Create a new domain
func (s *DomainsServiceFake) Create(createRequest *osc.DomainCreateRequest) (*osc.Domain, *osc.Response, error) {
	return FakeCreateFn(createRequest)
}

// Update a domain
func (s *DomainsServiceFake) Update(updateRequest *osc.DomainUpdateRequest) (*osc.Domain, *osc.Response, error) {
	return FakeUpdateFn(updateRequest)
}

// Delete domain
func (s *DomainsServiceFake) Delete(name string) (*osc.Response, error) {
	return FakeDeleteFn(name)
}

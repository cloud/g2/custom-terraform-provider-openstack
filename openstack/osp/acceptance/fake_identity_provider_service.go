package acceptance

import "main/openstack/osc"

type IdentityProvidersServiceFake struct {
}

var _ osc.IdentityProvidersService = &IdentityProvidersServiceFake{}

var fakeCreateIPFnDefault = func(createRequest *osc.IdentityProviderCreateRequest) (*osc.IdentityProvider, *osc.Response, error) {
	return nil, nil, nil
}

var FakeCreateIPFn = fakeCreateIPFnDefault

var fakeListIPFnDefault = func(opt *osc.ListOptions) ([]osc.IdentityProvider, *osc.Response, error) {
	return nil, nil, nil
}
var FakeListIPFn = fakeListIPFnDefault

var fakeGetIPFnDefault = func(id string) (*osc.IdentityProvider, *osc.Response, error) {
	return nil, nil, nil
}

var FakeGetIPFn = fakeGetIPFnDefault

var fakeUpdateIPFnDefault = func(updateRequest *osc.IdentityProviderUpdateRequest) (*osc.IdentityProvider, *osc.Response, error) {
	return nil, nil, nil
}
var FakeUpdateIPFn = fakeUpdateIPFnDefault

var fakeDeleteIPFnDefault = func(name string) (*osc.Response, error) {
	return nil, nil
}
var FakeDeleteIPFn = fakeDeleteIPFnDefault

func ResetFakeIP() {
	FakeCreateIPFn = fakeCreateIPFnDefault
	FakeListIPFn = fakeListIPFnDefault
	FakeGetIPFn = fakeGetIPFnDefault
	FakeUpdateIPFn = fakeUpdateIPFnDefault
	FakeDeleteIPFn = fakeDeleteIPFnDefault
}

func (s IdentityProvidersServiceFake) List(opt *osc.ListOptions) ([]osc.IdentityProvider, *osc.Response, error) {
	return FakeListIPFn(opt)
}

func (s IdentityProvidersServiceFake) Get(id string) (*osc.IdentityProvider, *osc.Response, error) {
	return FakeGetIPFn(id)
}

func (s *IdentityProvidersServiceFake) Create(createRequest *osc.IdentityProviderCreateRequest) (*osc.IdentityProvider, *osc.Response, error) {
	return FakeCreateIPFn(createRequest)
}

func (s *IdentityProvidersServiceFake) Update(updateRequest *osc.IdentityProviderUpdateRequest) (*osc.IdentityProvider, *osc.Response, error) {
	return FakeUpdateIPFn(updateRequest)
}

func (s *IdentityProvidersServiceFake) Delete(name string) (*osc.Response, error) {
	return FakeDeleteIPFn(name)
}

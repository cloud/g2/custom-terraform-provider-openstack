package acceptance

import "main/openstack/osc"

type FederationProtocolsServiceFake struct {
}

var _ osc.FederationProtocolsService = &FederationProtocolsServiceFake{}

var fakeCreateFPFnDefault = func(createRequest *osc.FederationProtocolCreateRequest) (*osc.FederationProtocol, *osc.Response, error) {
	return nil, nil, nil
}

var FakeCreateFPFn = fakeCreateFPFnDefault

var fakeListFPFnDefault = func(idp_id string, opt *osc.ListOptions) ([]osc.FederationProtocol, *osc.Response, error) {
	return nil, nil, nil
}
var FakeListFPFn = fakeListFPFnDefault

var fakeGetFPFnDefault = func(idp_id, id string) (*osc.FederationProtocol, *osc.Response, error) {
	return nil, nil, nil
}

var FakeGetFPFn = fakeGetFPFnDefault

var fakeUpdateFPFnDefault = func(updateRequest *osc.FederationProtocolUpdateRequest) (*osc.FederationProtocol, *osc.Response, error) {
	return nil, nil, nil
}
var FakeUpdateFPFn = fakeUpdateFPFnDefault

var fakeDeleteFPFnDefault = func(idp_id string, name string) (*osc.Response, error) {
	return nil, nil
}
var FakeDeleteFPFn = fakeDeleteFPFnDefault

func ResetFakeFP() {
	FakeCreateFPFn = fakeCreateFPFnDefault
	FakeListFPFn = fakeListFPFnDefault
	FakeGetFPFn = fakeGetFPFnDefault
	FakeUpdateFPFn = fakeUpdateFPFnDefault
	FakeDeleteFPFn = fakeDeleteFPFnDefault
}

func (s FederationProtocolsServiceFake) List(idp_id string, opt *osc.ListOptions) ([]osc.FederationProtocol, *osc.Response, error) {
	return FakeListFPFn(idp_id, opt)
}

func (s FederationProtocolsServiceFake) Get(idp_id string, id string) (*osc.FederationProtocol, *osc.Response, error) {
	return FakeGetFPFn(idp_id, id)
}

func (s *FederationProtocolsServiceFake) Create(createRequest *osc.FederationProtocolCreateRequest) (*osc.FederationProtocol, *osc.Response, error) {
	return FakeCreateFPFn(createRequest)
}

func (s *FederationProtocolsServiceFake) Update(updateRequest *osc.FederationProtocolUpdateRequest) (*osc.FederationProtocol, *osc.Response, error) {
	return FakeUpdateFPFn(updateRequest)
}

func (s *FederationProtocolsServiceFake) Delete(idp_id string, name string) (*osc.Response, error) {
	return FakeDeleteFPFn(idp_id, name)
}

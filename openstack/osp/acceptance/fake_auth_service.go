package acceptance

import (
	"fmt"
	"main/openstack/osc"
)

type AuthServiceFake struct {
}

var _ osc.AuthService = &AuthServiceFake{}

var fakeCreateAuthTokenDefault = func(createRequest *osc.TokenCreateRequest) (*osc.AuthToken, *osc.Response, error) {
	return nil, nil, fmt.Errorf("fakeCreateFnDefault not implemented")
}

var FakeCreateAuthTokenFn = fakeCreateAuthTokenDefault

func (s *AuthServiceFake) Create(createRequest *osc.TokenCreateRequest) (*osc.AuthToken, *osc.Response, error) {
	return FakeCreateAuthTokenFn(createRequest)
}

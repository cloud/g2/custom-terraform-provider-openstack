package mapping

import (
	"fmt"
	"github.com/hashicorp/terraform-plugin-sdk/helper/schema"
	"log"
	"main/openstack/osc"
	"main/openstack/osp/config"
)

func ResourceOpenStackMapping() *schema.Resource {
	return &schema.Resource{
		Create: resourceOpenStackMappingCreate,
		Read:   resourceOpenStackMappingRead,
		Delete: resourceOpenStackMappingDelete,
		Update: resourceOpenStackMappingUpdate,
		//Importer: &schema.ResourceImporter{
		//	State: schema.ImportStatePassthrough,
		//},
		Schema: map[string]*schema.Schema{
			"mapping_id": {
				Type:     schema.TypeString,
				Required: true,
			},
			"rules": {
				Type:     schema.TypeList,
				Required: true,
				Elem: &schema.Resource{
					Schema: map[string]*schema.Schema{
						"local": {
							Type:     schema.TypeList,
							Required: true,
							Elem: &schema.Resource{
								Schema: map[string]*schema.Schema{
									"user": {
										Type:     schema.TypeList,
										Optional: true,
										Elem: &schema.Resource{
											Schema: map[string]*schema.Schema{
												"name": {
													Type:     schema.TypeString,
													Required: true,
												},
												"email": {
													Type:     schema.TypeString,
													Required: true,
												},
											},
										},
									},
									"group": {
										Type:     schema.TypeList,
										Optional: true,
										Elem: &schema.Resource{
											Schema: map[string]*schema.Schema{
												"name": {
													Type:     schema.TypeString,
													Required: true,
												},
												"domain": {
													Type:     schema.TypeList,
													Required: true,
													Elem: &schema.Resource{
														Schema: map[string]*schema.Schema{
															"id": {
																Type:     schema.TypeString,
																Required: true,
															},
														},
													},
												},
											},
										},
									},
									"projects": {
										Type:     schema.TypeList,
										Optional: true,
										Elem: &schema.Resource{
											Schema: map[string]*schema.Schema{
												"name": {
													Type:     schema.TypeString,
													Required: true,
												},
												"roles": {
													Type:     schema.TypeList,
													Required: true,
													Elem: &schema.Resource{
														Schema: map[string]*schema.Schema{
															"name": {
																Type:     schema.TypeString,
																Required: true,
															},
														},
													},
												},
											},
										},
									},
								},
							},
						},
						"remote": {
							Type:     schema.TypeList,
							Required: true,
							Elem: &schema.Resource{
								Schema: map[string]*schema.Schema{
									"type": {
										Type:     schema.TypeString,
										Required: true,
									},
									"any_one_of": {
										Type:     schema.TypeList,
										Optional: true,
										Elem: &schema.Schema{
											Type: schema.TypeString,
										},
									},
								},
							},
						},
					},
				},
			},
		},
	}
}

func resourceOpenStackMappingCreate(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*config.CombinedConfig).OssClient()

	// Build up our creation options

	opts := &osc.MappingCreateRequest{
		Mapping: osc.MappingCreateRequestPart{
			ID:    d.Get("mapping_id").(string),
			Rules: expandRules(d.Get("rules").([]interface{})),
		},
	}

	log.Printf("[DEBUG] Mapping create configuration: %#v", opts)
	mapping, _, err := client.Mappings.Create(opts)
	if err != nil {
		return fmt.Errorf("error creating mapping: %s", err)
	}

	d.SetId(mapping.ID)

	log.Printf("[INFO] mapping ID: %s", mapping.ID)

	return resourceOpenStackMappingRead(d, meta)
}

func expandRules(rules []interface{}) []*osc.Rule {
	expandedRules := make([]*osc.Rule, 0, len(rules))

	for _, rawRule := range rules {
		rule := rawRule.(map[string]interface{})

		expandedRule := &osc.Rule{
			Local:  expandLocal(rule["local"].([]interface{})),
			Remote: expandRemote(rule["remote"].([]interface{})),
		}

		expandedRules = append(expandedRules, expandedRule)
	}

	return expandedRules
}

func expandLocal(locals []interface{}) []*osc.Local {
	expandedLocals := make([]*osc.Local, 0, len(locals))

	for _, rawLocal := range locals {
		local := rawLocal.(map[string]interface{})

		expandedLocal := &osc.Local{
			Group: expandGroup(local["group"].([]interface{})),
			User:  expandUser(local["user"].([]interface{})),
		}

		expandedLocals = append(expandedLocals, expandedLocal)
	}
	return expandedLocals
}

func expandUser(users []interface{}) *osc.User {
	for _, rawUser := range users {
		return &osc.User{
			Name:  rawUser.(map[string]interface{})["name"].(string),
			Email: rawUser.(map[string]interface{})["email"].(string),
		}
	}

	return nil
}

func expandGroup(groups []interface{}) *osc.Group {

	for _, rawGroup := range groups {
		group := rawGroup.(map[string]interface{})

		return &osc.Group{
			Name:   group["name"].(string),
			Domain: expandDomainReference(group["domain"].([]interface{})),
		}
	}

	return nil
}

func expandDomainReference(domainReferences []interface{}) *osc.DomainReference {
	for _, rawDomain := range domainReferences {
		domain := rawDomain.(map[string]interface{})

		expandedDomain := &osc.DomainReference{}

		domainID := domain["id"]
		if domainID != nil {
			domainID := domainID.(string)

			expandedDomain.ID = &domainID
		}

		domainName := domain["name"]
		if domainName != nil {
			domainName := domainName.(string)

			expandedDomain.Name = &domainName
		}

		return expandedDomain
	}

	return nil
}

//func expandDomainReference(domainReference interface{}) *osc.DomainReference {
//	rawDomain := domainReference(map[string]interface{})["domain"]
//
//	return &osc.DomainReference{
//		ID: rawDomain["id"].(string),
//	}
//
//	return nil
//}

func expandRemote(remotes []interface{}) []*osc.Remote {
	expandedRemote := make([]*osc.Remote, 0, len(remotes))

	for _, rawRemote := range remotes {
		remote := rawRemote.(map[string]interface{})

		r := &osc.Remote{
			Type: remote["type"].(string),
		}

		expandedRemote = append(expandedRemote, r)
	}

	return expandedRemote
}

func resourceOpenStackMappingUpdate(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*config.CombinedConfig).OssClient()

	// Build up our update options

	opts := &osc.MappingUpdateRequest{
		Mapping: osc.MappingUpdateRequestPart{
			ID:    d.Id(),
			Rules: expandRules(d.Get("rules").([]interface{})),
		},
	}

	log.Printf("[DEBUG] mapping update configuration: %#v", opts)
	mapping, _, err := client.Mappings.Update(opts)
	if err != nil {
		return fmt.Errorf("error updating mapping: %s", err)
	}

	log.Printf("[INFO] mapping ID: %s", mapping.ID)

	return resourceOpenStackMappingRead(d, meta)
}

func resourceOpenStackMappingRead(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*config.CombinedConfig).OssClient()

	mapping, resp, err := client.Mappings.Get(d.Id())
	if err != nil {
		// If the mapping is somehow already destroyed, mark as
		// successfully gone
		if resp != nil && resp.StatusCode == 404 {
			d.SetId("")
			return nil
		}

		return fmt.Errorf("error retrieving mapping: %s", err)
	}

	for ii, r := range mapping.Rules {
		for jj, l := range r.Local {

			if l.User != nil {
				d.Set(fmt.Sprintf("rules.%d.local.%d.user.name", ii, jj), l.User.Name)
				d.Set(fmt.Sprintf("rules.%d.local.%d.user.email", ii, jj), l.User.Email)
			}

			if l.Group != nil {
				d.Set(fmt.Sprintf("rules.%d.local.%d.group.name", ii, jj), l.Group.Name)

				if l.Group.Domain != nil {
					d.Set(fmt.Sprintf("rules.%d.local.%d.group.domain.id", ii, jj), l.Group.Domain.ID)
				}
			}

			for kk, p := range l.Projects {
				d.Set(fmt.Sprintf("rules.%d.local.%d.projects.%d.name", ii, jj, kk), p.Name)

				for oo, pr := range p.Roles {
					d.Set(fmt.Sprintf("rules.%d.local.%d.projects.%d.roles.%d.name", ii, jj, kk, oo), pr.Name)
				}

			}

		}

		for jj, remote := range r.Remote {

			d.Set(fmt.Sprintf("rules.%d.remote.%d.type", ii, jj), remote.Type)
			d.Set(fmt.Sprintf("rules.%d.remote.%d.any_one_of", ii, jj), remote.AnyOneOf)

		}

	}

	//d.Set("name", mapping.Name)
	//d.Set("description", mapping.Description)
	//d.Set("enabled", mapping.Enabled)

	return nil
}

func resourceOpenStackMappingDelete(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*config.CombinedConfig).OssClient()

	log.Printf("[INFO] Deleting mapping: %s", d.Id())
	_, err := client.Mappings.Delete(d.Id())
	if err != nil {
		return fmt.Errorf("error deleting mapping: %s", err)
	}

	d.SetId("")
	//d.Set("name", "")
	//d.Set("description", "")
	return nil
}

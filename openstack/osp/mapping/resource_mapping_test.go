package mapping_test

import (
	"github.com/hashicorp/terraform-plugin-sdk/helper/resource"
	"github.com/hashicorp/terraform-plugin-sdk/helper/schema"
	"main/openstack/osc"
	"main/openstack/osp/acceptance"
	"main/openstack/osp/config"
	"testing"
)

//

//

func TestSimpleCreate(t *testing.T) {
	acceptance.TestAccProvider.ConfigureFunc = func(data *schema.ResourceData) (interface{}, error) {
		return &config.CombinedConfig{
			Client: &osc.Client{
				Mappings: &acceptance.MappingsServiceFake{},
			},
		}, nil
	}

	acceptance.FakeCreateMappingFn = func(createRequest *osc.MappingCreateRequest) (*osc.Mapping, *osc.Response, error) {

		//if createRequest.Mapping.ID != "" {
		//	return nil, nil, fmt.Errorf("domain id cannot be specified: %s", createRequest.Domain.ID)
		//}
		//
		//if createRequest.Domain.Description == "test.com" {
		//	return nil, nil, fmt.Errorf("domain name is not correct: %s", createRequest.Domain.Name)
		//}

		return &osc.Mapping{
			ID: "mapping-test-id",
			Rules: []*osc.Rule{
				{
					Local: []*osc.Local{
						{
							User: &osc.User{
								Name:  "{0}",
								Email: "{1}",
							},
						},
					},
					Remote: []*osc.Remote{
						{
							Type: "OIDC-sub",
						},
						{
							Type: "OIDC-email",
						},
					},
				},
			},
		}, nil, nil
	}

	acceptance.FakeGetMappingFn = func(id string) (*osc.Mapping, *osc.Response, error) {
		return &osc.Mapping{
			ID: "mapping-test-id",
			Rules: []*osc.Rule{
				{
					Local: []*osc.Local{
						{
							User: &osc.User{
								Name:  "{0}",
								Email: "{1}",
							},
						},
					},
					Remote: []*osc.Remote{
						{
							Type: "OIDC-sub",
						},
						{
							Type: "OIDC-email",
						},
					},
				},
			},
		}, nil, nil
	}

	resource.Test(t, resource.TestCase{
		PreCheck:          func() { acceptance.TestAccPreCheck(t) },
		ProviderFactories: acceptance.TestAccProviderFactories,
		Steps: []resource.TestStep{
			{
				Config: test_openstack_mapping,
				Check: resource.ComposeTestCheckFunc(
					resource.TestCheckResourceAttr("openstack_mapping.testing_mapping", "id", "mapping-test-id"),
					resource.TestCheckResourceAttr("openstack_mapping.testing_mapping", "rules.0.local.0.user.0.name", "{0}"),
					resource.TestCheckResourceAttr("openstack_mapping.testing_mapping", "rules.0.local.0.user.0.email", "{1}"),
					resource.TestCheckResourceAttr("openstack_mapping.testing_mapping", "rules.0.remote.0.type", "OIDC-sub"),
					resource.TestCheckResourceAttr("openstack_mapping.testing_mapping", "rules.0.remote.1.type", "OIDC-email"),
				),
			},
		},
	})
}

func Test_simple_create_group(t *testing.T) {
	acceptance.TestAccProvider.ConfigureFunc = func(data *schema.ResourceData) (interface{}, error) {
		return &config.CombinedConfig{
			Client: &osc.Client{
				Mappings: &acceptance.MappingsServiceFake{},
			},
		}, nil
	}

	acceptance.FakeCreateMappingFn = func(createRequest *osc.MappingCreateRequest) (*osc.Mapping, *osc.Response, error) {

		//if createRequest.Mapping.ID != "" {
		//	return nil, nil, fmt.Errorf("domain id cannot be specified: %s", createRequest.Domain.ID)
		//}
		//
		//if createRequest.Domain.Description == "test.com" {
		//	return nil, nil, fmt.Errorf("domain name is not correct: %s", createRequest.Domain.Name)
		//}

		domainID := "0b2320313906481688277bbe99304732"

		return &osc.Mapping{
			ID: "mapping-test-id",
			Rules: []*osc.Rule{
				{
					Local: []*osc.Local{
						{
							Group: &osc.Group{
								Name: "e_infra_cz_federated_users",
								Domain: &osc.DomainReference{
									ID: &domainID,
								},
							},
						},
					},
					Remote: []*osc.Remote{
						{
							Type: "OIDC-group",
						},
					},
				},
			},
		}, nil, nil
	}

	acceptance.FakeGetMappingFn = func(id string) (*osc.Mapping, *osc.Response, error) {
		domainID := "0b2320313906481688277bbe99304732"

		return &osc.Mapping{
			ID: "mapping-test-id",
			Rules: []*osc.Rule{
				{
					Local: []*osc.Local{
						{
							Group: &osc.Group{
								Name: "e_infra_cz_federated_users",
								Domain: &osc.DomainReference{
									ID: &domainID,
								},
							},
						},
					},
					Remote: []*osc.Remote{
						{
							Type: "OIDC-group",
						},
					},
				},
			},
		}, nil, nil
	}

	resource.Test(t, resource.TestCase{
		PreCheck:          func() { acceptance.TestAccPreCheck(t) },
		ProviderFactories: acceptance.TestAccProviderFactories,
		Steps: []resource.TestStep{
			{
				Config: test_openstack_mapping_group,
				Check: resource.ComposeTestCheckFunc(
					resource.TestCheckResourceAttr("openstack_mapping.testing_mapping", "id", "mapping-test-id"),
					resource.TestCheckResourceAttr("openstack_mapping.testing_mapping", "rules.0.local.0.group.0.name", "e_infra_cz_federated_users"),
					resource.TestCheckResourceAttr("openstack_mapping.testing_mapping", "rules.0.local.0.group.0.domain.0.id", "0b2320313906481688277bbe99304732"),
					resource.TestCheckResourceAttr("openstack_mapping.testing_mapping", "rules.0.remote.0.type", "OIDC-group"),
				),
			},
		},
	})
}

func Test_simple_create_projects(t *testing.T) {
	acceptance.TestAccProvider.ConfigureFunc = func(data *schema.ResourceData) (interface{}, error) {
		return &config.CombinedConfig{
			Client: &osc.Client{
				Mappings: &acceptance.MappingsServiceFake{},
			},
		}, nil
	}

	acceptance.FakeCreateMappingFn = func(createRequest *osc.MappingCreateRequest) (*osc.Mapping, *osc.Response, error) {

		//if createRequest.Mapping.ID != "" {
		//	return nil, nil, fmt.Errorf("domain id cannot be specified: %s", createRequest.Domain.ID)
		//}
		//
		//if createRequest.Domain.Description == "test.com" {
		//	return nil, nil, fmt.Errorf("domain name is not correct: %s", createRequest.Domain.Name)
		//}

		return &osc.Mapping{
			ID: "mapping-test-id",
			Rules: []*osc.Rule{
				{
					Local: []*osc.Local{
						{
							Projects: []*osc.Project{
								{
									Name: "{0}",
									Roles: []*osc.Role{
										{Name: "member"},
										{Name: "creator"},
									},
								},
							},
						},
					},
					Remote: []*osc.Remote{
						{
							Type: "OIDC-group",
						},
						{
							Type: "OIDC-email",
						},
						{
							Type: "OIDC-eduperson_entitlement",
						},
					},
				},
			},
		}, nil, nil
	}

	acceptance.FakeGetMappingFn = func(id string) (*osc.Mapping, *osc.Response, error) {
		return &osc.Mapping{
			ID: "mapping-test-id",
			Rules: []*osc.Rule{
				{
					Local: []*osc.Local{
						{
							Projects: []*osc.Project{
								{
									Name: "{0}",
									Roles: []*osc.Role{
										{Name: "member"},
										{Name: "creator"},
									},
								},
							},
						},
					},
					Remote: []*osc.Remote{
						{
							Type: "OIDC-group",
						},
						{
							Type: "OIDC-email",
						},
						{
							Type: "OIDC-eduperson_entitlement",
						},
					},
				},
			},
		}, nil, nil
	}

	resource.Test(t, resource.TestCase{
		PreCheck:          func() { acceptance.TestAccPreCheck(t) },
		ProviderFactories: acceptance.TestAccProviderFactories,
		Steps: []resource.TestStep{
			{
				Config: test_openstack_mapping_local_projects,
				Check: resource.ComposeTestCheckFunc(
					resource.TestCheckResourceAttr("openstack_mapping.testing_mapping", "id", "mapping-test-id"),
					resource.TestCheckResourceAttr("openstack_mapping.testing_mapping", "rules.0.local.0.projects.0.name", "{0}"),
					resource.TestCheckResourceAttr("openstack_mapping.testing_mapping", "rules.0.local.0.projects.0.roles.0.name", "member"),
					resource.TestCheckResourceAttr("openstack_mapping.testing_mapping", "rules.0.local.0.projects.0.roles.1.name", "creator"),
					resource.TestCheckResourceAttr("openstack_mapping.testing_mapping", "rules.0.remote.0.type", "OIDC-sub"),
					resource.TestCheckResourceAttr("openstack_mapping.testing_mapping", "rules.0.remote.1.type", "OIDC-email"),
					resource.TestCheckResourceAttr("openstack_mapping.testing_mapping", "rules.0.remote.2.type", "OIDC-eduperson_entitlement"),
				),
			},
		},
	})
}

func Test_simple_create_with_remote(t *testing.T) {
	acceptance.TestAccProvider.ConfigureFunc = func(data *schema.ResourceData) (interface{}, error) {
		return &config.CombinedConfig{
			Client: &osc.Client{
				Mappings: &acceptance.MappingsServiceFake{},
			},
		}, nil
	}

	acceptance.FakeCreateMappingFn = func(createRequest *osc.MappingCreateRequest) (*osc.Mapping, *osc.Response, error) {

		//if createRequest.Mapping.ID != "" {
		//	return nil, nil, fmt.Errorf("domain id cannot be specified: %s", createRequest.Domain.ID)
		//}
		//
		//if createRequest.Domain.Description == "test.com" {
		//	return nil, nil, fmt.Errorf("domain name is not correct: %s", createRequest.Domain.Name)
		//}

		return &osc.Mapping{
			ID: "mapping-test-id",
			Rules: []*osc.Rule{
				{
					Local: []*osc.Local{
						{
							Projects: []*osc.Project{
								{
									Name: "ostack-demo",
									Roles: []*osc.Role{
										{Name: "member"},
										{Name: "creator"},
										{Name: "admin"},
									},
								},
							},
						},
					},
					Remote: []*osc.Remote{
						{
							Type:     "OIDC-sub",
							AnyOneOf: []string{"c2bf29961b887b399a456269bbcb7aedd3127a26@einfra.cesnet.cz"},
						},
					},
				},
			},
		}, nil, nil
	}

	acceptance.FakeGetMappingFn = func(id string) (*osc.Mapping, *osc.Response, error) {
		return &osc.Mapping{
			ID: "mapping-test-id",
			Rules: []*osc.Rule{
				{
					Local: []*osc.Local{
						{
							Projects: []*osc.Project{
								{
									Name: "ostack-demo",
									Roles: []*osc.Role{
										{Name: "member"},
										{Name: "creator"},
										{Name: "admin"},
									},
								},
							},
						},
					},
					Remote: []*osc.Remote{
						{
							Type:     "OIDC-sub",
							AnyOneOf: []string{"c2bf29961b887b399a456269bbcb7aedd3127a26@einfra.cesnet.cz"},
						},
					},
				},
			},
		}, nil, nil
	}

	resource.Test(t, resource.TestCase{
		PreCheck:          func() { acceptance.TestAccPreCheck(t) },
		ProviderFactories: acceptance.TestAccProviderFactories,
		Steps: []resource.TestStep{
			{
				Config: test_openstack_mapping_with_remote,
				Check: resource.ComposeTestCheckFunc(
					resource.TestCheckResourceAttr("openstack_mapping.testing_mapping", "id", "mapping-test-id"),
					resource.TestCheckResourceAttr("openstack_mapping.testing_mapping", "rules.0.local.0.projects.0.name", "{0}"),
					resource.TestCheckResourceAttr("openstack_mapping.testing_mapping", "rules.0.local.0.projects.0.roles.0.name", "member"),
					resource.TestCheckResourceAttr("openstack_mapping.testing_mapping", "rules.0.local.0.projects.0.roles.1.name", "creator"),
					resource.TestCheckResourceAttr("openstack_mapping.testing_mapping", "rules.0.local.0.projects.0.roles.2.name", "admin"),
					resource.TestCheckResourceAttr("openstack_mapping.testing_mapping", "rules.0.remote.0.type", "OIDC-sub"),
					resource.TestCheckResourceAttr("openstack_mapping.testing_mapping", "rules.0.remote.0.any_one_of.0", "c2bf29961b887b399a456269bbcb7aedd3127a26@einfra.cesnet.cz"),
				),
			},
		},
	})
}

//func TestCreateWithExplicitID(t *testing.T) {
//	expectedName := "test.com"
//	expectedDescription := "test description"
//	expectedEnabled := "true"
//	expectedExplicitID := "explicit-id-example"
//
//	acceptance.TestAccProvider.ConfigureFunc = func(data *schema.ResourceData) (interface{}, error) {
//		return &config.CombinedConfig{
//			Client: &osc.Client{
//				Domains: &acceptance.DomainsServiceFake{},
//			},
//		}, nil
//	}
//
//	acceptance.FakeCreateFn = func(createRequest *osc.DomainCreateRequest) (*osc.Domain, *osc.Response, error) {
//
//		assert.Equal(t, createRequest.Domain.ExplicitDomainID, expectedExplicitID)
//
//		return &osc.Domain{
//			ID:          expectedExplicitID,
//			Name:        "test.com",
//			Description: "test description",
//			Enabled:     true,
//		}, nil, nil
//	}
//
//	acceptance.FakeGetFn = func(id string) (*osc.Domain, *osc.Response, error) {
//		return &osc.Domain{
//			ID:          expectedExplicitID,
//			Name:        "test.com",
//			Description: "test description",
//			Enabled:     true,
//		}, nil, nil
//	}
//
//	resource.Test(t, resource.TestCase{
//		PreCheck:          func() { acceptance.TestAccPreCheck(t) },
//		ProviderFactories: acceptance.TestAccProviderFactories,
//		Steps: []resource.TestStep{
//			{
//				Config: fmt.Sprintf(testDomainWithExplicitID, expectedName),
//				Check: resource.ComposeTestCheckFunc(
//					resource.TestCheckResourceAttr(
//						"openstack_domain.foobar", "id", expectedExplicitID),
//					resource.TestCheckResourceAttr(
//						"openstack_domain.foobar", "name", expectedName),
//					resource.TestCheckResourceAttr(
//						"openstack_domain.foobar", "description", expectedDescription),
//					resource.TestCheckResourceAttr(
//						"openstack_domain.foobar", "enabled", expectedEnabled),
//				),
//			},
//		},
//	})
//}
//
//func Test_create_with_disabled(t *testing.T) {
//	expectedName := "test.com"
//	expectedDescription := "test description"
//	expectedEnabled := "false"
//
//	acceptance.TestAccProvider.ConfigureFunc = func(data *schema.ResourceData) (interface{}, error) {
//		return &config.CombinedConfig{
//			Client: &osc.Client{
//				Domains: &acceptance.DomainsServiceFake{},
//			},
//		}, nil
//	}
//
//	acceptance.FakeCreateFn = func(createRequest *osc.DomainCreateRequest) (*osc.Domain, *osc.Response, error) {
//
//		return &osc.Domain{
//			ID:          "123",
//			Name:        "test.com",
//			Description: "test description",
//			Enabled:     false,
//		}, nil, nil
//	}
//
//	acceptance.FakeGetFn = func(id string) (*osc.Domain, *osc.Response, error) {
//		return &osc.Domain{
//			ID:          "123",
//			Name:        "test.com",
//			Description: "test description",
//			Enabled:     false,
//		}, nil, nil
//	}
//
//	resource.Test(t, resource.TestCase{
//		PreCheck:          func() { acceptance.TestAccPreCheck(t) },
//		ProviderFactories: acceptance.TestAccProviderFactories,
//		Steps: []resource.TestStep{
//			{
//				Config: fmt.Sprintf(testDomainDisabled, expectedName),
//				Check: resource.ComposeTestCheckFunc(
//					resource.TestCheckResourceAttr(
//						"openstack_domain.foobar", "id", "123"),
//					resource.TestCheckResourceAttr(
//						"openstack_domain.foobar", "name", expectedName),
//					resource.TestCheckResourceAttr(
//						"openstack_domain.foobar", "description", expectedDescription),
//					resource.TestCheckResourceAttr(
//						"openstack_domain.foobar", "enabled", expectedEnabled),
//				),
//			},
//		},
//	})
//}

const test_openstack_mapping = `
resource "openstack_mapping" "testing_mapping" {
	
	mapping_id = "mapping-test-id"

	rules {

		local {

			user {
				name = "{0}"
				email = "{1}"
			}

		}

		remote {
			type = "OIDC-sub"
		}

		remote {
			type = "OIDC-email"
		}
	}
}`

const test_openstack_mapping_group = `
resource "openstack_mapping" "testing_mapping" {
	
	mapping_id = "mapping-test-id"

	rules {

		local {

			group {
				name = "e_infra_cz_federated_users" 
				domain {
					id = "0b2320313906481688277bbe99304732"
				}
			}

		}

		remote {
			type = "OIDC-group"
		}
	}
}`

const test_openstack_mapping_local_projects = `
resource "openstack_mapping" "testing_mapping" {
	
	mapping_id = "mapping-test-id"

	rules {

		local {

			projects {

				name = "{0}"
				
				roles {
					name = "member"
				}

				roles {	
					name = "creator"
				}

			}
		}

		remote {
			type = "OIDC-sub"
		}

		remote {
			type = "OIDC-email"
		}

		remote {
			type = "OIDC-eduperson_entitlement"
		}
	}
}`

const test_openstack_mapping_with_remote = `
resource "openstack_mapping" "testing_mapping" {
	
	mapping_id = "mapping-test-id"

	rules {

		local {

			projects {

				name = "{0}"
				
				roles {
					name = "member"
				}

				roles {	
					name = "creator"
				}

				roles {	
					name = "admin"
				}

			}
		}

		remote {
			type = "OIDC-sub"
			any_one_of = ["c2bf29961b887b399a456269bbcb7aedd3127a26@einfra.cesnet.cz"]
		}

	}
}`

//const testDomainWithExplicitID = `
//resource "openstack_domain" "foobar" {
// explicit_id = "explicit-id-example"
// name       = "%s"
// description = "test description"
// enabled = true
//}`
//
//const testDomainDisabled = `
//resource "openstack_domain" "foobar" {
// name       = "%s"
// description = "test description"
// enabled = false
//}`

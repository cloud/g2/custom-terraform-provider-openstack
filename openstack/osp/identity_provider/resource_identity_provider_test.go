package identity_provider_test

import (
	"github.com/hashicorp/terraform-plugin-sdk/helper/resource"
	"github.com/hashicorp/terraform-plugin-sdk/helper/schema"
	"main/openstack/osc"
	"main/openstack/osp/acceptance"
	"main/openstack/osp/config"
	"testing"
)

//

//

func TestSimpleCreate(t *testing.T) {

	expectedDescription := "testing description"

	acceptance.TestAccProvider.ConfigureFunc = func(data *schema.ResourceData) (interface{}, error) {
		return &config.CombinedConfig{
			Client: &osc.Client{
				IdentityProviders: &acceptance.IdentityProvidersServiceFake{},
			},
		}, nil
	}

	acceptance.FakeCreateIPFn = func(createRequest *osc.IdentityProviderCreateRequest) (*osc.IdentityProvider, *osc.Response, error) {
		return &osc.IdentityProvider{
			ID:               "ACME",
			DomainID:         "testing_domain_id",
			Description:      expectedDescription,
			Enabled:          true,
			RemoteIDS:        []string{"https://login.e-infra.cz/oidc/", "y"},
			AuthorizationTTL: 10,
		}, nil, nil
	}

	acceptance.FakeGetIPFn = func(id string) (*osc.IdentityProvider, *osc.Response, error) {
		return &osc.IdentityProvider{
			ID:               "ACME",
			DomainID:         "testing_domain_id",
			Description:      expectedDescription,
			Enabled:          true,
			RemoteIDS:        []string{"https://login.e-infra.cz/oidc/", "y"},
			AuthorizationTTL: 10,
		}, nil, nil
	}

	resource.Test(t, resource.TestCase{
		PreCheck:          func() { acceptance.TestAccPreCheck(t) },
		ProviderFactories: acceptance.TestAccProviderFactories,
		Steps: []resource.TestStep{
			{
				Config: test_ip_default,

				Check: resource.ComposeTestCheckFunc(
					resource.TestCheckResourceAttr("openstack_identity_provider.acme", "id", "ACME"),
					resource.TestCheckResourceAttr("openstack_identity_provider.acme", "domain_id", "testing_domain_id"),
					resource.TestCheckResourceAttr("openstack_identity_provider.acme", "description", expectedDescription),
					resource.TestCheckResourceAttr("openstack_identity_provider.acme", "enabled", "true"),
					resource.TestCheckResourceAttr("openstack_identity_provider.acme", "remote_ids.0", "https://login.e-infra.cz/oidc/"),
					resource.TestCheckResourceAttr("openstack_identity_provider.acme", "remote_ids.1", "y"),
					resource.TestCheckResourceAttr("openstack_identity_provider.acme", "authorization_ttl", "10"),
				),
			},
		},
	})
}

//func TestCreateWithExplicitID(t *testing.T) {
//	expectedName := "test.com"
//	expectedDescription := "test description"
//	expectedEnabled := "true"
//	expectedExplicitID := "explicit-id-example"
//
//	acceptance.TestAccProvider.ConfigureFunc = func(data *schema.ResourceData) (interface{}, error) {
//		return &config.CombinedConfig{
//			Client: &osc.Client{
//				Domains: &acceptance.DomainsServiceFake{},
//			},
//		}, nil
//	}
//
//	acceptance.FakeCreateFn = func(createRequest *osc.DomainCreateRequest) (*osc.Domain, *osc.Response, error) {
//
//		assert.Equal(t, createRequest.Domain.ExplicitDomainID, expectedExplicitID)
//
//		return &osc.Domain{
//			ID:          expectedExplicitID,
//			Name:        "test.com",
//			Description: "test description",
//			Enabled:     true,
//		}, nil, nil
//	}
//
//	acceptance.FakeGetFn = func(id string) (*osc.Domain, *osc.Response, error) {
//		return &osc.Domain{
//			ID:          expectedExplicitID,
//			Name:        "test.com",
//			Description: "test description",
//			Enabled:     true,
//		}, nil, nil
//	}
//
//	resource.Test(t, resource.TestCase{
//		PreCheck:          func() { acceptance.TestAccPreCheck(t) },
//		ProviderFactories: acceptance.TestAccProviderFactories,
//		Steps: []resource.TestStep{
//			{
//				Config: fmt.Sprintf(testDomainWithExplicitID, expectedName),
//				Check: resource.ComposeTestCheckFunc(
//					resource.TestCheckResourceAttr(
//						"openstack_identity_provider.acme", "id", expectedExplicitID),
//					resource.TestCheckResourceAttr(
//						"openstack_identity_provider.acme", "name", expectedName),
//					resource.TestCheckResourceAttr(
//						"openstack_identity_provider.acme", "description", expectedDescription),
//					resource.TestCheckResourceAttr(
//						"openstack_identity_provider.acme", "enabled", expectedEnabled),
//				),
//			},
//		},
//	})
//}
//
//func Test_create_with_disabled(t *testing.T) {
//	expectedName := "test.com"
//	expectedDescription := "test description"
//	expectedEnabled := "false"
//
//	acceptance.TestAccProvider.ConfigureFunc = func(data *schema.ResourceData) (interface{}, error) {
//		return &config.CombinedConfig{
//			Client: &osc.Client{
//				Domains: &acceptance.DomainsServiceFake{},
//			},
//		}, nil
//	}
//
//	acceptance.FakeCreateFn = func(createRequest *osc.DomainCreateRequest) (*osc.Domain, *osc.Response, error) {
//
//		return &osc.Domain{
//			ID:          "123",
//			Name:        "test.com",
//			Description: "test description",
//			Enabled:     false,
//		}, nil, nil
//	}
//
//	acceptance.FakeGetFn = func(id string) (*osc.Domain, *osc.Response, error) {
//		return &osc.Domain{
//			ID:          "123",
//			Name:        "test.com",
//			Description: "test description",
//			Enabled:     false,
//		}, nil, nil
//	}
//
//	resource.Test(t, resource.TestCase{
//		PreCheck:          func() { acceptance.TestAccPreCheck(t) },
//		ProviderFactories: acceptance.TestAccProviderFactories,
//		Steps: []resource.TestStep{
//			{
//				Config: fmt.Sprintf(testDomainDisabled, expectedName),
//				Check: resource.ComposeTestCheckFunc(
//					resource.TestCheckResourceAttr(
//						"openstack_identity_provider.acme", "id", "123"),
//					resource.TestCheckResourceAttr(
//						"openstack_identity_provider.acme", "name", expectedName),
//					resource.TestCheckResourceAttr(
//						"openstack_identity_provider.acme", "description", expectedDescription),
//					resource.TestCheckResourceAttr(
//						"openstack_identity_provider.acme", "enabled", expectedEnabled),
//				),
//			},
//		},
//	})
//}

const test_ip_default = `
resource "openstack_identity_provider" "acme" {
	identity_provider_id = "ACME"
	domain_id = "testing_domain_id"
	enabled = true
	description = "testing description"
	authorization_ttl = 10
	remote_ids = ["https://login.e-infra.cz/oidc/", "y"]
}
`

package identity_provider

import (
	"fmt"
	"github.com/hashicorp/terraform-plugin-sdk/helper/schema"
	"log"
	"main/openstack/osc"
	"main/openstack/osp/config"
)

func ResourceOpenStackIdentityProvider() *schema.Resource {
	return &schema.Resource{
		Create: resourceOpenStackIdentityProviderCreate,
		Read:   resourceOpenStackIdentityProviderRead,
		Delete: resourceOpenStackIdentityProviderDelete,
		Update: resourceOpenStackIdentityProviderUpdate,
		//Importer: &schema.ResourceImporter{
		//	State: schema.ImportStatePassthrough,
		//},
		Schema: map[string]*schema.Schema{
			"identity_provider_id": {
				Type:     schema.TypeString,
				Required: true,
			},
			"domain_id": {
				Type:     schema.TypeString,
				Required: true,
			},
			"enabled": {
				Type:     schema.TypeBool,
				Optional: true,
				Default:  true,
			},
			"description": {
				Type:     schema.TypeString,
				Optional: true,
			},
			"authorization_ttl": {
				Type:     schema.TypeInt,
				Optional: true,
			},
			"remote_ids": {
				Type:     schema.TypeList,
				Optional: true,
				Elem: &schema.Schema{
					Type: schema.TypeString,
				},
			},
		},
	}
}

func resourceOpenStackIdentityProviderCreate(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*config.CombinedConfig).OssClient()
	opts := &osc.IdentityProviderCreateRequest{}

	// required
	opts.IdentityProvider.DomainID = d.Get("domain_id").(string)
	opts.IdentityProvider.ID = d.Get("identity_provider_id").(string)

	// optional
	if v, ok := d.GetOk("description"); ok {
		opts.IdentityProvider.Description = v.(string)
	}

	if v, ok := d.GetOk("enabled"); ok {
		opts.IdentityProvider.Enabled = v.(bool)
	}

	if v, ok := d.GetOk("authorization_ttl"); ok {
		opts.IdentityProvider.AuthorizationTTL = v.(int)
	}

	if v, ok := d.GetOk("remote_ids"); ok {
		remoteIDS := make([]string, 0)

		for _, kk := range v.([]interface{}) {
			remoteIDS = append(remoteIDS, kk.(string))
		}

		opts.IdentityProvider.RemoteIDS = remoteIDS
	}

	log.Printf("[DEBUG] IdentityProvider create configuration: %#v", opts)
	ip, _, err := client.IdentityProviders.Create(opts)

	if err != nil {
		return fmt.Errorf("error creating Domain: %s", err)
	}

	d.SetId(ip.ID)

	log.Printf("[INFO] IdentityProvider ID: %s", ip.ID)

	return resourceOpenStackIdentityProviderRead(d, meta)
}

func resourceOpenStackIdentityProviderUpdate(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*config.CombinedConfig).OssClient()

	opts := &osc.IdentityProviderUpdateRequest{
		IdentityProvider: osc.IdentityProviderUpdateRequestPart{
			ID: d.Id(),
		},
	}

	if v, ok := d.GetOk("description"); ok {
		opts.IdentityProvider.Description = v.(string)
	}

	if v, ok := d.GetOk("enabled"); ok {
		opts.IdentityProvider.Enabled = v.(bool)
	}

	if v, ok := d.GetOk("authorization_ttl"); ok {
		opts.IdentityProvider.AuthorizationTTL = v.(int)
	}

	if v, ok := d.GetOk("remote_ids"); ok {
		opts.IdentityProvider.RemoteIDS = v.([]string)
	}

	log.Printf("[DEBUG] IdentityProvider update configuration: %#v", opts)
	ip, _, err := client.IdentityProviders.Update(opts)

	if err != nil {
		return fmt.Errorf("error creating IdentityProvider: %s", err)
	}

	log.Printf("[INFO] IdentityProvider ID: %s", ip.ID)

	return resourceOpenStackIdentityProviderRead(d, meta)
}

func resourceOpenStackIdentityProviderRead(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*config.CombinedConfig).OssClient()

	ip, resp, err := client.IdentityProviders.Get(d.Id())
	if err != nil {
		// If the domain is somehow already destroyed, mark as
		// successfully gone
		if resp != nil && resp.StatusCode == 404 {
			d.SetId("")
			return nil
		}

		return fmt.Errorf("error retrieving identity provider: %s", err)
	}

	d.Set("authorization_ttl", ip.AuthorizationTTL)
	d.Set("description", ip.Description)
	d.Set("domain_id", ip.DomainID)
	d.Set("enabled", ip.Enabled)
	d.Set("remote_ids", ip.RemoteIDS)

	return nil
}

func resourceOpenStackIdentityProviderDelete(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*config.CombinedConfig).OssClient()

	log.Printf("[INFO] identity provider: %s", d.Id())
	_, err := client.IdentityProviders.Delete(d.Id())
	if err != nil {
		return fmt.Errorf("error deleting identity provider: %s", err)
	}

	d.SetId("")
	d.Set("domain_id", "")
	d.Set("description", "")
	d.Set("enabled", false)
	d.Set("remote_ids", "")
	d.Set("authorization_ttl", "")

	return nil
}

package osc

import (
	"encoding/json"
	"fmt"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"net/http"
	"testing"
)

func Test_simple_get_all_mappings(t *testing.T) {
	setup()
	defer teardown()

	mux.HandleFunc("/v3/OS-FEDERATION/mappings", func(w http.ResponseWriter, r *http.Request) {
		testMethod(t, r, http.MethodGet)
		fmt.Fprint(w, `{
    "links": {
        "next": null,
        "previous": null,
        "self": "http://example.com/identity/v3/OS-FEDERATION/mappings"
    },
    "mappings": [
        {
            "id": "openstack_v1_rules",
            "rules": [
                {
                    "local": [
                        {
                            "user": {
                                "name": "{0}",
                                "email": "{1}"
                            }
                        }
                    ],
                    "remote": [
                        {
                            "type": "OIDC-sub"
                        },
                        {
                            "type": "OIDC-email"
                        }
                    ]
                },
                {
                    "local": [
                        {
                            "projects": [
                                {
                                    "name": "{0}",
                                    "roles": [
                                        {
                                            "name": "member"
                                        },
                                        {
                                            "name": "heat_stack_owner"
                                        },
                                        {
                                            "name": "creator"
                                        }
                                    ]
                                }
                            ]
                        }
                    ],
                    "remote": [
                        {
                            "type": "OIDC-sub"
                        },
                        {
                            "type": "OIDC-email"
                        },
                        {
                            "type": "OIDC-eduperson_entitlement",
                            "any_one_of": [
                                "urn:geant:cesnet.cz:res:individuals#perun.cesnet.cz",
                                "urn:geant:e-infra.cz:res:individuals#perun.e-infra.cz"
                            ]
                        }
                    ]
                },
                {
                    "local": [
                        {
                            "projects": [
                                {
                                    "name": "admin",
                                    "roles": [
                                        {
                                            "name": "admin"
                                        },
                                        {
                                            "name": "heat_stack_owner"
                                        },
                                        {
                                            "name": "image_uploader"
                                        }
                                    ]
                                }
                            ]
                        }
                    ],
                    "remote": [
                        {
                            "type": "OIDC-eduperson_entitlement",
                            "any_one_of": [
                                "urn:geant:cesnet.cz:res:individuals#perun.cesnet.cz",
                                "urn:geant:e-infra.cz:res:individuals#perun.e-infra.cz"
                            ]
                        }
                    ]
                }
            ]
        }
    ]
}`)
	})

	mappings, _, err := client.Mappings.List(nil)
	require.NoError(t, err)

	expectedMappings := []Mapping{
		{
			ID: "openstack_v1_rules",
			Rules: []*Rule{
				// 1st rule
				{
					Local: []*Local{
						{
							User: &User{
								Name:  "{0}",
								Email: "{1}",
							},
						},
					},
					Remote: []*Remote{
						{Type: "OIDC-sub"},
						{Type: "OIDC-email"},
					},
				},

				// 2nd rule
				{
					Local: []*Local{
						{
							Projects: []*Project{
								{
									Name: "{0}",
									Roles: []*Role{
										{Name: "member"},
										{Name: "heat_stack_owner"},
										{Name: "creator"},
									},
								},
							},
						},
					},
					Remote: []*Remote{
						{Type: "OIDC-sub"},
						{Type: "OIDC-email"},
						{
							Type: "OIDC-eduperson_entitlement",
							AnyOneOf: []string{
								"urn:geant:cesnet.cz:res:individuals#perun.cesnet.cz",
								"urn:geant:e-infra.cz:res:individuals#perun.e-infra.cz",
							},
						},
					},
				},

				// 3rd rule
				{
					Local: []*Local{
						{
							Projects: []*Project{
								{
									Name: "admin",
									Roles: []*Role{
										{Name: "admin"},
										{Name: "heat_stack_owner"},
										{Name: "image_uploader"},
									},
								},
							},
						},
					},
					Remote: []*Remote{
						{
							Type: "OIDC-eduperson_entitlement",
							AnyOneOf: []string{
								"urn:geant:cesnet.cz:res:individuals#perun.cesnet.cz",
								"urn:geant:e-infra.cz:res:individuals#perun.e-infra.cz",
							},
						},
					},
				},
			},
		},
	}

	assert.Equal(t, expectedMappings, mappings)
}

func Test_simple_mapping_get(t *testing.T) {
	setup()
	defer teardown()

	mux.HandleFunc("/v3/OS-FEDERATION/mappings/openstack_v1_rule", func(w http.ResponseWriter, r *http.Request) {
		testMethod(t, r, http.MethodGet)
		fmt.Fprint(w, `{
    "mapping": {
        "id": "openstack_v1_rule",
        "rules": [
            {
                "local": [
                    {
                        "user": {
                            "name": "{0}",
                            "email": "{1}"
                        }
                    }
                ],
                "remote": [
                    {
                        "type": "OIDC-sub"
                    },
                    {
                        "type": "OIDC-email"
                    }
                ]
            }
        ]
    }
}`)
	})

	mapping, _, err := client.Mappings.Get("openstack_v1_rule")
	require.NoError(t, err)

	expected := &Mapping{
		ID: "openstack_v1_rule",
		Rules: []*Rule{
			{
				Local: []*Local{
					{
						User: &User{
							Name:  "{0}",
							Email: "{1}",
						},
					},
				},
				Remote: []*Remote{
					{Type: "OIDC-sub"},
					{Type: "OIDC-email"},
				},
			},
		},
	}
	assert.Equal(t, expected, mapping)
}

func Test_simple_create_mapping(t *testing.T) {
	setup()
	defer teardown()

	createRequest := &MappingCreateRequest{
		Mapping: MappingCreateRequestPart{
			ID: "openstack_v2_rule",
			Rules: []*Rule{
				{
					Local: []*Local{
						{
							User: &User{
								Name:  "{0}",
								Email: "{1}",
							},
						},
					},
					Remote: []*Remote{
						{Type: "OIDC-sub"},
						{Type: "OIDC-email"},
					},
				},
			},
		},
	}

	mux.HandleFunc("/v3/OS-FEDERATION/mappings/openstack_v2_rule", func(w http.ResponseWriter, r *http.Request) {
		v := new(MappingCreateRequest)
		err := json.NewDecoder(r.Body).Decode(v)
		if err != nil {
			t.Fatal(err)
		}

		testMethod(t, r, http.MethodPut)
		//assert.Equal(t, createRequest, v)

		fmt.Fprint(w, `{
    "mapping": {
        "id": "openstack_v2_rule",
        "rules": [
            {
                "local": [
                    {
                        "user": {
                            "name": "{0}",
                            "email": "{1}"
                        }
                    }
                ],
                "remote": [
                    {
                        "type": "OIDC-sub"
                    },
                    {
                        "type": "OIDC-email"
                    }
                ]
            }
        ]
    }
}`)
	})

	mapping, _, err := client.Mappings.Create(createRequest)
	require.NoError(t, err)

	expected := &Mapping{
		ID: "openstack_v2_rule",
		Rules: []*Rule{
			{
				Local: []*Local{
					{
						User: &User{
							Name:  "{0}",
							Email: "{1}",
						},
					},
				},
				Remote: []*Remote{
					{Type: "OIDC-sub"},
					{Type: "OIDC-email"},
				},
			},
		},
	}
	assert.Equal(t, expected, mapping)
}

func Test_simple_delete_mapping(t *testing.T) {
	setup()
	defer teardown()

	mux.HandleFunc("/v3/OS-FEDERATION/mappings/openstack_v2_rule", func(w http.ResponseWriter, r *http.Request) {
		testMethod(t, r, http.MethodDelete)
	})

	_, err := client.Mappings.Delete("openstack_v2_rule")

	assert.NoError(t, err)
}

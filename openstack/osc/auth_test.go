package osc

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"net/http"
	"testing"
)

func Test_auth_with_app_credentials(t *testing.T) {
	setup()
	defer teardown()

	mux.HandleFunc("/auth/tokens", func(w http.ResponseWriter, r *http.Request) {
		testMethod(t, r, http.MethodPost)

		w.Header().Set("X-Subject-Token", "1d1d1d1d1d1d1d1d1d1d1d1d1d1d1")

		fmt.Fprint(w, `{
    "token": {
        "methods": [
            "application_credential"
        ],
        "expires_at": "2015-11-06T15:32:17.893769Z",
        "user": {
            "domain": {
                "id": "default",
                "name": "Default"
            },
            "id": "423f19a4ac1e4f48bbb4180756e6eb6c",
            "name": "admin",
            "password_expires_at": null
        },
        "audit_ids": [
            "ZzZwkUflQfygX7pdYDBCQQ"
        ],
        "issued_at": "2015-11-06T14:32:17.893797Z"
    }
}`)
	})

	createTokenFromPasswordRequest := &TokenCreateRequest{
		Auth: TokenCreateRequestPart{
			Identity: TokenCreateRequestIdentity{
				Methods: []string{"application_credential"},
				ApplicationCredential: &ApplicationCredential{
					ID:     "app_cred_id",
					Secret: "app_cred_secret",
				},
			},
		},
	}

	token, _, err := client.AuthService.Create(createTokenFromPasswordRequest)
	require.NoError(t, err)

	expectedToken := &AuthToken{
		TokenID:   "1d1d1d1d1d1d1d1d1d1d1d1d1d1d1",
		Methods:   []string{"application_credential"},
		ExpiresAt: "2015-11-06T15:32:17.893769Z",
		User: &AuthUser{
			ID:   "423f19a4ac1e4f48bbb4180756e6eb6c",
			Name: "admin",
			Domain: &AuthDomain{
				ID:   "default",
				Name: "Default",
			},
		},
		AuditIDs: []string{"ZzZwkUflQfygX7pdYDBCQQ"},
		IssuedAt: "2015-11-06T14:32:17.893797Z",
	}

	assert.Equal(t, expectedToken, token)
}

func Test_auth_with_password(t *testing.T) {
	setup()
	defer teardown()

	mux.HandleFunc("/auth/tokens", func(w http.ResponseWriter, r *http.Request) {
		testMethod(t, r, http.MethodPost)

		w.Header().Set("X-Subject-Token", "1d1d1d1d1d1d1d1d1d1d1d1d1d1d1")

		fmt.Fprint(w, `{
    "token": {
        "methods": [
            "password"
        ],
        "expires_at": "2015-11-06T15:32:17.893769Z",
        "user": {
            "domain": {
                "id": "default",
                "name": "Default"
            },
            "id": "423f19a4ac1e4f48bbb4180756e6eb6c",
            "name": "admin",
            "password_expires_at": null
        },
        "audit_ids": [
            "ZzZwkUflQfygX7pdYDBCQQ"
        ],
        "issued_at": "2015-11-06T14:32:17.893797Z"
    }
}`)
	})

	createTokenFromPasswordRequest := &TokenCreateRequest{
		Auth: TokenCreateRequestPart{
			Identity: TokenCreateRequestIdentity{
				Methods: []string{"password"},
				Password: &Password{
					User: &AuthUser{
						Name:     "username",
						Password: "password",
						Domain:   &AuthDomain{Name: "domain"},
					},
				},
			},
		},
	}

	token, _, err := client.AuthService.Create(createTokenFromPasswordRequest)
	require.NoError(t, err)

	expectedToken := &AuthToken{
		TokenID:   "1d1d1d1d1d1d1d1d1d1d1d1d1d1d1",
		Methods:   []string{"password"},
		ExpiresAt: "2015-11-06T15:32:17.893769Z",
		User: &AuthUser{
			ID:   "423f19a4ac1e4f48bbb4180756e6eb6c",
			Name: "admin",
			Domain: &AuthDomain{
				ID:   "default",
				Name: "Default",
			},
		},
		AuditIDs: []string{"ZzZwkUflQfygX7pdYDBCQQ"},
		IssuedAt: "2015-11-06T14:32:17.893797Z",
	}

	assert.Equal(t, expectedToken, token)
}

//func TestDomains_GetDomain(t *testing.T) {
//	setup()
//	defer teardown()
//
//	mux.HandleFunc("/v3/domains/1", func(w http.ResponseWriter, r *http.Request) {
//		testMethod(t, r, http.MethodGet)
//		fmt.Fprint(w, `{"domain":{"id":"1","name":"example.com"}}`)
//	})
//
//	domains, _, err := client.Domains.Get("1")
//	require.NoError(t, err)
//
//	expected := &Domain{ID: "1", Name: "example.com"}
//	assert.Equal(t, expected, domains)
//}
//
//func TestDomains_Create(t *testing.T) {
//	setup()
//	defer teardown()
//
//	createRequest := &DomainCreateRequest{
//		Domain: DomainCreateRequestPart{
//			Name:        "example.com",
//			Description: "example domain test",
//		},
//	}
//
//	mux.HandleFunc("/v3/domains", func(w http.ResponseWriter, r *http.Request) {
//		v := new(DomainCreateRequest)
//		err := json.NewDecoder(r.Body).Decode(v)
//		if err != nil {
//			t.Fatal(err)
//		}
//
//		testMethod(t, r, http.MethodPost)
//		assert.Equal(t, createRequest, v)
//
//		fmt.Fprint(w, `{"domain":{"id":"10","name":"example.com","description":"example domain test"}}`)
//	})
//
//	domain, _, err := client.Domains.Create(createRequest)
//	require.NoError(t, err)
//
//	expected := &Domain{ID: "10", Name: "example.com", Description: "example domain test"}
//	assert.Equal(t, expected, domain)
//}
//
//func TestDomains_CreateShouldThrowWithoutRequest(t *testing.T) {
//	setup()
//	defer teardown()
//
//	_, _, err := client.Domains.Create(nil)
//	require.Error(t, err)
//}
//
//func TestDomains_Destroy(t *testing.T) {
//	setup()
//	defer teardown()
//
//	mux.HandleFunc("/v3/domains/10", func(w http.ResponseWriter, r *http.Request) {
//		testMethod(t, r, http.MethodDelete)
//	})
//
//	_, err := client.Domains.Delete("10")
//
//	assert.NoError(t, err)
//}

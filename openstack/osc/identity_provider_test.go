package osc

import (
	"encoding/json"
	"fmt"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"net/http"
	"testing"
)

func Test_simple_get_all_identity_providers(t *testing.T) {
	setup()
	defer teardown()

	mux.HandleFunc("/v3/OS-FEDERATION/identity_providers", func(w http.ResponseWriter, r *http.Request) {
		testMethod(t, r, http.MethodGet)
		fmt.Fprint(w, `{
    "identity_providers": [
        {
            "domain_id": "1789d1",
            "description": "Stores ACME identities",
            "remote_ids": ["acme_id_1", "acme_id_2"],
            "enabled": true,
            "id": "ACME",
            "links": {
                "protocols": "http://example.com/identity/v3/OS-FEDERATION/identity_providers/ACME/protocols",
                "self": "http://example.com/identity/v3/OS-FEDERATION/identity_providers/ACME"
            }
        },
        {
            "domain_id": "2890e2",
            "description": "Stores contractor identities",
            "remote_ids": ["store_id_1", "store_id_2"],
            "enabled": false,
            "id": "ACME-contractors",
            "links": {
                "protocols": "http://example.com/identity/v3/OS-FEDERATION/identity_providers/ACME-contractors/protocols",
                "self": "http://example.com/identity/v3/OS-FEDERATION/identity_providers/ACME-contractors"
            }
        }
    ],
    "links": {
        "next": null,
        "previous": null,
        "self": "http://example.com/identity/v3/OS-FEDERATION/identity_providers"
    }
}`)
	})

	ips, resp, err := client.IdentityProviders.List(nil)
	require.NoError(t, err)

	expectedIps := []IdentityProvider{
		{ID: "ACME", DomainID: "1789d1", Description: "Stores ACME identities", RemoteIDS: []string{"acme_id_1", "acme_id_2"}, Enabled: true},
		{ID: "ACME-contractors", DomainID: "2890e2", Description: "Stores contractor identities", RemoteIDS: []string{"store_id_1", "store_id_2"}, Enabled: false},
	}

	assert.Equal(t, expectedIps, ips)

	expectedLinks := &Links{
		Self: "http://example.com/identity/v3/OS-FEDERATION/identity_providers",
	}

	assert.Equal(t, expectedLinks, resp.Links)
}

func Test_get_single_ip(t *testing.T) {
	setup()
	defer teardown()

	mux.HandleFunc("/v3/OS-FEDERATION/identity_providers/ACME", func(w http.ResponseWriter, r *http.Request) {
		testMethod(t, r, http.MethodGet)
		fmt.Fprint(w, `{
    "identity_provider": {
        "authorization_ttl": null,
        "domain_id": "1789d1",
        "description": "Stores ACME identities",
        "remote_ids": ["acme_id_1", "acme_id_2"],
        "enabled": false,
        "id": "ACME",
        "links": {
            "protocols": "http://example.com/identity/v3/OS-FEDERATION/identity_providers/ACME/protocols",
            "self": "http://example.com/identity/v3/OS-FEDERATION/identity_providers/ACME"
        }
    }
}`)
	})

	ip, _, err := client.IdentityProviders.Get("ACME")
	require.NoError(t, err)

	expectedIp := &IdentityProvider{
		ID:          "ACME",
		DomainID:    "1789d1",
		Description: "Stores ACME identities",
		RemoteIDS:   []string{"acme_id_1", "acme_id_2"},
		Enabled:     false,
	}

	assert.Equal(t, expectedIp, ip)
}

func Test_simple_create(t *testing.T) {
	setup()
	defer teardown()

	createRequest := &IdentityProviderCreateRequest{
		IdentityProvider: IdentityProviderCreateRequestPart{
			ID:          "TEST",
			DomainID:    "TEST-DOMAIN",
			Description: "testing description",
			RemoteIDS:   []string{"id1", "id2"},
			Enabled:     true,
		},
	}

	mux.HandleFunc("/v3/OS-FEDERATION/identity_providers/TEST", func(w http.ResponseWriter, r *http.Request) {
		v := new(IdentityProviderCreateRequest)
		err := json.NewDecoder(r.Body).Decode(v)
		if err != nil {
			t.Fatal(err)
		}

		testMethod(t, r, http.MethodPut)
		assert.Equal(t, createRequest, v)

		fmt.Fprint(w, `{
    "identity_provider": {
		"id": "TEST",
        "domain_id": "TEST-DOMAIN",
        "description": "testing description",
        "remote_ids": ["id1", "id2"],
        "enabled": false
    }
}`)
	})

	ip, _, err := client.IdentityProviders.Create(createRequest)
	require.NoError(t, err)

	expected := &IdentityProvider{
		ID:          "TEST",
		DomainID:    "TEST-DOMAIN",
		Description: "testing description",
		RemoteIDS:   []string{"id1", "id2"},
		Enabled:     false,
	}

	assert.Equal(t, expected, ip)
}

//func TestDomains_CreateShouldThrowWithoutRequest(t *testing.T) {
//	setup()
//	defer teardown()
//
//	_, _, err := client.Domains.Create(nil)
//	require.Error(t, err)
//}
//
//func TestDomains_Destroy(t *testing.T) {
//	setup()
//	defer teardown()
//
//	mux.HandleFunc("/v3/domains/10", func(w http.ResponseWriter, r *http.Request) {
//		testMethod(t, r, http.MethodDelete)
//	})
//
//	_, err := client.Domains.Delete("10")
//
//	assert.NoError(t, err)
//}

package osc

import (
	"fmt"
	"net/http"
)

const domainsBasePath = "v3/domains"

type DomainsService interface {
	List(*ListOptions) ([]Domain, *Response, error)
	Get(string) (*Domain, *Response, error)
	Create(*DomainCreateRequest) (*Domain, *Response, error)
	Delete(string) (*Response, error)
	Update(*DomainUpdateRequest) (*Domain, *Response, error)
}

// DomainsServiceOp handles communication with the domain related methods of the
// OpenStack API.
type DomainsServiceOp struct {
	client *Client
}

var _ DomainsService = &DomainsServiceOp{}

// Domain represents a OpenStack domain
type Domain struct {
	ID          string `json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`
	Enabled     bool   `json:"enabled"`
}

type domainRoot struct {
	Domain *Domain `json:"domain"`
}

type domainsRoot struct {
	Domains []Domain `json:"domains"`
	Links   *Links   `json:"links"`
}

type DomainCreateRequest struct {
	Domain DomainCreateRequestPart `json:"domain"`
}

type DomainUpdateRequest struct {
	Domain DomainUpdateRequestPart `json:"domain"`
}

type DomainUpdateRequestPart struct {
	ID string `json:"-,omitempty"`

	// The name of the domain.
	Name string `json:"name"`

	// The description of the domain.
	Description string `json:"description,omitempty"`

	// If set to true, domain is created enabled. If set to false, domain is created disabled. The default is true.
	// Users can only authorize against an enabled domain (and any of its projects).
	// In addition, users can only authenticate if the domain that owns them is also enabled.
	// Disabling a domain prevents both of these things.
	Enabled bool `json:"enabled"`
}

type DomainCreateRequestPart struct {
	ID string `json:"-,omitempty"`

	// The name of the domain.
	Name string `json:"name"`

	// The description of the domain.
	Description string `json:"description,omitempty"`

	// If set to true, domain is created enabled. If set to false, domain is created disabled. The default is true.
	// Users can only authorize against an enabled domain (and any of its projects).
	// In addition, users can only authenticate if the domain that owns them is also enabled.
	// Disabling a domain prevents both of these things.
	Enabled bool `json:"enabled"`

	//// The resource options for the domain. Available resource options are immutable.
	//Options CreateDomainCommandOptions `json:"options"`

	// The ID of the domain. A domain created this way will not use an auto-generated ID,
	// but will use the ID passed in instead.
	// Identifiers passed in this way must conform to the existing ID generation scheme: UUID4 without dashes.
	ExplicitDomainID string `json:"explicit_domain_id,omitempty"`
}

// List returns a list of domains
func (s DomainsServiceOp) List(opt *ListOptions) ([]Domain, *Response, error) {
	path := domainsBasePath
	path, err := addOptions(path, opt)
	if err != nil {
		return nil, nil, err
	}

	req, err := s.client.NewRequest("GET", path, nil)
	if err != nil {
		return nil, nil, err
	}

	root := new(domainsRoot)
	resp, err := s.client.Do(req, root)
	if err != nil {
		return nil, resp, err
	}
	if l := root.Links; l != nil {
		resp.Links = l
	}

	return root.Domains, resp, err
}

// Get returns a domain
func (s DomainsServiceOp) Get(id string) (*Domain, *Response, error) {
	if len(id) < 1 {
		return nil, nil, NewArgError("id", "cannot be empty")
	}

	path := fmt.Sprintf("%s/%s", domainsBasePath, id)

	req, err := s.client.NewRequest("GET", path, nil)
	if err != nil {
		return nil, nil, err
	}

	root := new(domainRoot)
	resp, err := s.client.Do(req, root)
	if err != nil {
		return nil, resp, err
	}

	return root.Domain, resp, err
}

// Create a new domain
func (s *DomainsServiceOp) Create(createRequest *DomainCreateRequest) (*Domain, *Response, error) {
	if createRequest == nil {
		return nil, nil, NewArgError("createRequest", "cannot be nil")
	}

	path := domainsBasePath

	req, err := s.client.NewRequest(http.MethodPost, path, createRequest)
	if err != nil {
		return nil, nil, err
	}

	root := new(domainRoot)
	resp, err := s.client.Do(req, root)
	if err != nil {
		return nil, resp, err
	}
	return root.Domain, resp, err
}

// Update a domain
func (s *DomainsServiceOp) Update(updateRequest *DomainUpdateRequest) (*Domain, *Response, error) {
	if updateRequest == nil {
		return nil, nil, NewArgError("updateRequest", "cannot be nil")
	}

	path := fmt.Sprintf("%s/%s", domainsBasePath, updateRequest.Domain.ID)

	req, err := s.client.NewRequest(http.MethodPatch, path, updateRequest)
	if err != nil {
		return nil, nil, err
	}

	root := new(domainRoot)
	resp, err := s.client.Do(req, root)
	if err != nil {
		return nil, resp, err
	}
	return root.Domain, resp, err
}

// Delete domain
func (s *DomainsServiceOp) Delete(name string) (*Response, error) {
	if len(name) < 1 {
		return nil, NewArgError("name", "cannot be an empty string")
	}

	path := fmt.Sprintf("%s/%s", domainsBasePath, name)

	req, err := s.client.NewRequest(http.MethodDelete, path, nil)
	if err != nil {
		return nil, err
	}

	resp, err := s.client.Do(req, nil)

	return resp, err
}

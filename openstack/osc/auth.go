package osc

import (
	"net/http"
)

type AuthService interface {
	Create(*TokenCreateRequest) (*AuthToken, *Response, error)
}

type AuthServiceOp struct {
	client *Client
}

var _ AuthService = &AuthServiceOp{}

type AuthToken struct {
	// returned in header
	TokenID string `json:"-"`

	// in request
	Methods   []string  `json:"methods"`
	ExpiresAt string    `json:"expires_at,omitempty"`
	User      *AuthUser `json:"user"`

	// in response only
	AuditIDs []string `json:"audit_ids,omitempty"`
	IssuedAt string   `json:"issued_at,omitempty"`
}

type AuthUser struct {
	Name     string      `json:"name"`
	Password string      `json:"password"`
	Domain   *AuthDomain `json:"domain"`

	// in response only
	ID                string `json:"id,omitempty"`
	PasswordExpiresAt string `json:"password_expires_at,omitempty"`
}

type AuthDomain struct {
	ID   string `json:"id,omitempty"`
	Name string `json:"name,omitempty"`
}

type tokenRoot struct {
	AuthToken *AuthToken `json:"token"`
}

type TokenCreateRequest struct {
	Auth TokenCreateRequestPart `json:"auth"`
}

type TokenCreateRequestPart struct {
	Identity TokenCreateRequestIdentity `json:"identity"`
	Scope    *TokenCreateRequestScope   `json:"scope,omitempty"`
}

type TokenCreateRequestScope struct {
	Project *TokenCreateRequestProject `json:"project,omitempty"`
}

type TokenCreateRequestProject struct {
	ID     string      `json:"id,omitempty"`
	Name   string      `json:"name,omitempty"`
	Domain *AuthDomain `json:"domain,omitempty"`
}

type TokenCreateRequestIdentity struct {
	Methods               []string               `json:"methods"`
	ApplicationCredential *ApplicationCredential `json:"application_credential,omitempty"`
	Password              *Password              `json:"password,omitempty"`
}

type Password struct {
	User *AuthUser `json:"user"`
}

type ApplicationCredential struct {
	ID     string `json:"id"`
	Secret string `json:"secret"`
}

// Create a new domain
func (s *AuthServiceOp) Create(createRequest *TokenCreateRequest) (*AuthToken, *Response, error) {
	if createRequest == nil {
		return nil, nil, NewArgError("createRequest", "cannot be nil")
	}

	path := s.client.FullAuthURL.String() + "/auth/tokens"

	req, err := s.client.NewRequest(http.MethodPost, path, createRequest)
	if err != nil {
		return nil, nil, err
	}

	root := new(tokenRoot)
	resp, err := s.client.Do(req, root)
	if err != nil {
		return nil, resp, err
	}

	// obtain token id from header
	root.AuthToken.TokenID = resp.Header.Get("X-Subject-Token")

	return root.AuthToken, resp, err
}

package osc

import (
	"fmt"
	"net/http"
)

const identityProvidersBasePath = "/v3/OS-FEDERATION/identity_providers"

type IdentityProvidersService interface {
	List(*ListOptions) ([]IdentityProvider, *Response, error)
	Get(string) (*IdentityProvider, *Response, error)
	Create(request *IdentityProviderCreateRequest) (*IdentityProvider, *Response, error)
	Delete(string) (*Response, error)
	Update(*IdentityProviderUpdateRequest) (*IdentityProvider, *Response, error)
}

type IdentityProvidersServiceOp struct {
	client *Client
}

var _ IdentityProvidersService = &IdentityProvidersServiceOp{}

// Domain represents a OpenStack domain
type IdentityProvider struct {
	ID               string   `json:"id"`
	DomainID         string   `json:"domain_id"`
	Description      string   `json:"description,omitempty"`
	Enabled          bool     `json:"enabled"`
	AuthorizationTTL int      `json:"authorization_ttl,omitempty"`
	RemoteIDS        []string `json:"remote_ids,omitempty"`
}

type identityProviderRoot struct {
	IdentityProvider *IdentityProvider `json:"identity_provider"`
}

type identityProvidersRoot struct {
	IdentityProviders []IdentityProvider `json:"identity_providers"`
	Links             *Links             `json:"links"`
}

type IdentityProviderCreateRequest struct {
	IdentityProvider IdentityProviderCreateRequestPart `json:"identity_provider"`
}

type IdentityProviderUpdateRequest struct {
	IdentityProvider IdentityProviderUpdateRequestPart `json:"identity_provider"`
}

type IdentityProviderUpdateRequestPart struct {
	// path
	ID string `json:"-"`

	// Except domain_id, any attribute of an Identity Provider may be passed in the request body.
	//To update the domain_id, you will need to delete and recreate the Identity Provider.
	//If domain_id is included in the request, a 400 response code will be returned.
	//DomainID         string   `json:"domain_id"`

	// body params
	Description      string   `json:"description,omitempty"`
	Enabled          bool     `json:"enabled"`
	AuthorizationTTL int      `json:"authorization_ttl,omitempty"`
	RemoteIDS        []string `json:"remote_ids,omitempty"`
}

type IdentityProviderCreateRequestPart struct {
	// path
	ID string `json:"id"`

	// body
	DomainID         string   `json:"domain_id"`
	Description      string   `json:"description,omitempty"`
	Enabled          bool     `json:"enabled"`
	AuthorizationTTL int      `json:"authorization_ttl,omitempty"`
	RemoteIDS        []string `json:"remote_ids,omitempty"`
}

// List returns a list of domains
func (s IdentityProvidersServiceOp) List(opt *ListOptions) ([]IdentityProvider, *Response, error) {
	path := identityProvidersBasePath
	path, err := addOptions(path, opt)
	if err != nil {
		return nil, nil, err
	}

	req, err := s.client.NewRequest("GET", path, nil)
	if err != nil {
		return nil, nil, err
	}

	root := new(identityProvidersRoot)
	resp, err := s.client.Do(req, root)
	if err != nil {
		return nil, resp, err
	}
	if l := root.Links; l != nil {
		resp.Links = l
	}

	return root.IdentityProviders, resp, err
}

func (s IdentityProvidersServiceOp) Get(id string) (*IdentityProvider, *Response, error) {
	if len(id) < 1 {
		return nil, nil, NewArgError("id", "cannot be empty")
	}

	path := fmt.Sprintf("%s/%s", identityProvidersBasePath, id)

	req, err := s.client.NewRequest("GET", path, nil)
	if err != nil {
		return nil, nil, err
	}

	root := new(identityProviderRoot)
	resp, err := s.client.Do(req, root)
	if err != nil {
		return nil, resp, err
	}

	return root.IdentityProvider, resp, err
}

func (s *IdentityProvidersServiceOp) Create(createRequest *IdentityProviderCreateRequest) (*IdentityProvider, *Response, error) {
	if createRequest == nil {
		return nil, nil, NewArgError("createRequest", "cannot be nil")
	}

	path := fmt.Sprintf("%s/%s", identityProvidersBasePath, createRequest.IdentityProvider.ID)

	req, err := s.client.NewRequest(http.MethodPut, path, createRequest)
	if err != nil {
		return nil, nil, err
	}

	root := new(identityProviderRoot)
	resp, err := s.client.Do(req, root)
	if err != nil {
		return nil, resp, err
	}
	return root.IdentityProvider, resp, err
}

func (s *IdentityProvidersServiceOp) Update(updateRequest *IdentityProviderUpdateRequest) (*IdentityProvider, *Response, error) {
	if updateRequest == nil {
		return nil, nil, NewArgError("updateRequest", "cannot be nil")
	}

	path := fmt.Sprintf("%s/%s", identityProvidersBasePath, updateRequest.IdentityProvider.ID)

	req, err := s.client.NewRequest(http.MethodPatch, path, updateRequest)
	if err != nil {
		return nil, nil, err
	}

	root := new(identityProviderRoot)
	resp, err := s.client.Do(req, root)
	if err != nil {
		return nil, resp, err
	}
	return root.IdentityProvider, resp, err
}

func (s *IdentityProvidersServiceOp) Delete(name string) (*Response, error) {
	if len(name) < 1 {
		return nil, NewArgError("name", "cannot be an empty string")
	}

	path := fmt.Sprintf("%s/%s", identityProvidersBasePath, name)

	req, err := s.client.NewRequest(http.MethodDelete, path, nil)
	if err != nil {
		return nil, err
	}

	resp, err := s.client.Do(req, nil)

	return resp, err
}

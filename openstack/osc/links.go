package osc

import (
	"net/url"
	"strconv"
)

// Links manages links that are returned along with a List
type Links struct {
	First    string `json:"first,omitempty"`
	Previous string `json:"previous,omitempty"`
	Last     string `json:"last,omitempty"`
	Next     string `json:"next,omitempty"`
	Self     string `json:"self,omitempty"`
}

// CurrentPage is current page of the list
func (l *Links) CurrentPage() (int, error) {
	return l.current()
}

func (l *Links) current() (int, error) {
	switch {
	case l == nil:
		return 1, nil
	case l.Previous == "" && l.Next != "":
		return 1, nil
	case l.Previous != "":
		prevPage, err := pageForURL(l.Previous)
		if err != nil {
			return 0, err
		}

		return prevPage + 1, nil
	}

	return 0, nil
}

// IsLastPage returns true if the current page is the last
func (l *Links) IsLastPage() bool {
	return l.isLast()
}

func (l *Links) isLast() bool {
	return l.Next == ""
}

func pageForURL(urlText string) (int, error) {
	u, err := url.ParseRequestURI(urlText)
	if err != nil {
		return 0, err
	}

	pageStr := u.Query().Get("page")
	page, err := strconv.Atoi(pageStr)
	if err != nil {
		return 0, err
	}

	return page, nil
}

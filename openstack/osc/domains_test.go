package osc

import (
	"encoding/json"
	"fmt"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"net/http"
	"testing"
)

func TestDomains_ListDomains(t *testing.T) {
	setup()
	defer teardown()

	mux.HandleFunc("/v3/domains", func(w http.ResponseWriter, r *http.Request) {
		testMethod(t, r, http.MethodGet)
		fmt.Fprint(w, `{
			"domains": [
				{
					"id": "10",
					"name":"example.com"
				},
				{
					"id": "11",
					"name":"example2.com"
				}
			],
			"links": {
				"first": "first-link"
			}
		}`)
	})

	domains, resp, err := client.Domains.List(nil)
	require.NoError(t, err)

	expectedDomains := []Domain{{ID: "10", Name: "example.com"}, {ID: "11", Name: "example2.com"}}
	assert.Equal(t, expectedDomains, domains)

	expectedLinks := &Links{First: "first-link", Next: ""}
	assert.Equal(t, expectedLinks, resp.Links)
}

func TestDomains_GetDomain(t *testing.T) {
	setup()
	defer teardown()

	mux.HandleFunc("/v3/domains/1", func(w http.ResponseWriter, r *http.Request) {
		testMethod(t, r, http.MethodGet)
		fmt.Fprint(w, `{"domain":{"id":"1","name":"example.com"}}`)
	})

	domains, _, err := client.Domains.Get("1")
	require.NoError(t, err)

	expected := &Domain{ID: "1", Name: "example.com"}
	assert.Equal(t, expected, domains)
}

func TestDomains_Create(t *testing.T) {
	setup()
	defer teardown()

	createRequest := &DomainCreateRequest{
		Domain: DomainCreateRequestPart{
			Name:        "example.com",
			Description: "example domain test",
		},
	}

	mux.HandleFunc("/v3/domains", func(w http.ResponseWriter, r *http.Request) {
		v := new(DomainCreateRequest)
		err := json.NewDecoder(r.Body).Decode(v)
		if err != nil {
			t.Fatal(err)
		}

		testMethod(t, r, http.MethodPost)
		assert.Equal(t, createRequest, v)

		fmt.Fprint(w, `{"domain":{"id":"10","name":"example.com","description":"example domain test"}}`)
	})

	domain, _, err := client.Domains.Create(createRequest)
	require.NoError(t, err)

	expected := &Domain{ID: "10", Name: "example.com", Description: "example domain test"}
	assert.Equal(t, expected, domain)
}

func TestDomains_CreateShouldThrowWithoutRequest(t *testing.T) {
	setup()
	defer teardown()

	_, _, err := client.Domains.Create(nil)
	require.Error(t, err)
}

func TestDomains_Destroy(t *testing.T) {
	setup()
	defer teardown()

	mux.HandleFunc("/v3/domains/10", func(w http.ResponseWriter, r *http.Request) {
		testMethod(t, r, http.MethodDelete)
	})

	_, err := client.Domains.Delete("10")

	assert.NoError(t, err)
}

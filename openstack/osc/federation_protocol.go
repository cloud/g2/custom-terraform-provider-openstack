package osc

import (
	"fmt"
	"net/http"
)

type FederationProtocolsService interface {
	List(string, *ListOptions) ([]FederationProtocol, *Response, error)
	Get(string, string) (*FederationProtocol, *Response, error)
	Create(request *FederationProtocolCreateRequest) (*FederationProtocol, *Response, error)
	Delete(string, string) (*Response, error)
	Update(*FederationProtocolUpdateRequest) (*FederationProtocol, *Response, error)
}

type FederationProtocolsServiceOp struct {
	client *Client
}

var _ FederationProtocolsService = &FederationProtocolsServiceOp{}

type FederationProtocol struct {
	ID                string `json:"id"`
	IdentityProvider  string `json:"idp_id"`
	Mapping           string `json:"mapping_id"`
	RemoteIDAttribute string `json:"remote_id_attribute"`
}

type federationProtocolRoot struct {
	FederationProtocol *FederationProtocol `json:"protocol"`
}

type federationProtocolsRoot struct {
	FederationProtocols []FederationProtocol `json:"protocols"`
	Links               *Links               `json:"links"`
}

type FederationProtocolCreateRequest struct {
	FederationProtocol FederationProtocolCreateRequestPart `json:"protocol"`
}

type FederationProtocolUpdateRequest struct {
	FederationProtocol FederationProtocolUpdateRequestPart `json:"protocol"`
}

type FederationProtocolUpdateRequestPart struct {
	// path
	IdentityProvider string `json:"-"`
	ID               string `json:"-"`

	// body params
	Mapping           string `json:"mapping_id"`
	RemoteIDAttribute string `json:"remote_id_attribute"`
}

type FederationProtocolCreateRequestPart struct {
	// path
	IdentityProvider string `json:"-"`
	ID               string `json:"-"`

	// body
	Mapping           string `json:"mapping_id"`
	RemoteIDAttribute string `json:"remote_id_attribute"`
}

func (s FederationProtocolsServiceOp) List(idp_id string, opt *ListOptions) ([]FederationProtocol, *Response, error) {
	path := fmt.Sprintf("%s/%s/protocols", identityProvidersBasePath, idp_id)
	path, err := addOptions(path, opt)
	if err != nil {
		return nil, nil, err
	}

	req, err := s.client.NewRequest("GET", path, nil)
	if err != nil {
		return nil, nil, err
	}

	root := new(federationProtocolsRoot)
	resp, err := s.client.Do(req, root)
	if err != nil {
		return nil, resp, err
	}
	if l := root.Links; l != nil {
		resp.Links = l
	}

	return root.FederationProtocols, resp, err
}

func (s FederationProtocolsServiceOp) Get(idp_id string, id string) (*FederationProtocol, *Response, error) {
	if len(id) < 1 {
		return nil, nil, NewArgError("id", "cannot be empty")
	}

	path := fmt.Sprintf(
		"%s/%s/protocols/%s",
		identityProvidersBasePath,
		idp_id,
		id,
	)

	req, err := s.client.NewRequest("GET", path, nil)
	if err != nil {
		return nil, nil, err
	}

	root := new(federationProtocolRoot)
	resp, err := s.client.Do(req, root)
	if err != nil {
		return nil, resp, err
	}

	return root.FederationProtocol, resp, err
}

func (s *FederationProtocolsServiceOp) Create(createRequest *FederationProtocolCreateRequest) (*FederationProtocol, *Response, error) {
	if createRequest == nil {
		return nil, nil, NewArgError("createRequest", "cannot be nil")
	}

	path := fmt.Sprintf(
		"%s/%s/protocols/%s",
		identityProvidersBasePath,
		createRequest.FederationProtocol.IdentityProvider,
		createRequest.FederationProtocol.ID,
	)

	req, err := s.client.NewRequest(http.MethodPut, path, createRequest)
	if err != nil {
		return nil, nil, err
	}

	root := new(federationProtocolRoot)
	resp, err := s.client.Do(req, root)
	if err != nil {
		return nil, resp, err
	}
	return root.FederationProtocol, resp, err
}

func (s *FederationProtocolsServiceOp) Update(updateRequest *FederationProtocolUpdateRequest) (*FederationProtocol, *Response, error) {
	if updateRequest == nil {
		return nil, nil, NewArgError("updateRequest", "cannot be nil")
	}

	path := fmt.Sprintf(
		"%s/%s/protocols/%s",
		identityProvidersBasePath,
		updateRequest.FederationProtocol.IdentityProvider,
		updateRequest.FederationProtocol.ID,
	)

	req, err := s.client.NewRequest(http.MethodPatch, path, updateRequest)
	if err != nil {
		return nil, nil, err
	}

	root := new(federationProtocolRoot)
	resp, err := s.client.Do(req, root)
	if err != nil {
		return nil, resp, err
	}
	return root.FederationProtocol, resp, err
}

func (s *FederationProtocolsServiceOp) Delete(idp_id string, name string) (*Response, error) {
	if len(name) < 1 {
		return nil, NewArgError("name", "cannot be an empty string")
	}

	path := fmt.Sprintf(
		"%s/%s/protocols/%s",
		identityProvidersBasePath,
		idp_id,
		name,
	)

	req, err := s.client.NewRequest(http.MethodDelete, path, nil)
	if err != nil {
		return nil, err
	}

	resp, err := s.client.Do(req, nil)

	return resp, err
}

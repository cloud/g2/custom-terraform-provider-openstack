package osc

import (
	"fmt"
	"net/http"
)

const mappingBasePath = "/v3/OS-FEDERATION/mappings"

type MappingsService interface {
	List(*ListOptions) ([]Mapping, *Response, error)
	Get(string) (*Mapping, *Response, error)
	Create(*MappingCreateRequest) (*Mapping, *Response, error)
	Delete(string) (*Response, error)
	Update(*MappingUpdateRequest) (*Mapping, *Response, error)
}

type MappingsServiceOp struct {
	client *Client
}

var _ MappingsService = &MappingsServiceOp{}

type Mapping struct {
	ID    string  `json:"id"`
	Rules []*Rule `json:"rules"`
}

type Rule struct {
	Local  []*Local  `json:"local"`
	Remote []*Remote `json:"remote"`
}

type Local struct {
	User     *User      `json:"user,omitempty"`
	Group    *Group     `json:"group,omitempty"`
	Projects []*Project `json:"projects,omitempty"`
}

type Group struct {
	Name   string           `json:"name,omitempty"`
	Domain *DomainReference `json:"domain,omitempty"`
}

type DomainReference struct {
	ID   *string `json:"id,omitempty"`
	Name *string `json:"name,omitempty"`
}

type Project struct {
	Name  string  `json:"name"`
	Roles []*Role `json:"roles"`
}

type Role struct {
	Name string `json:"name"`
}

type User struct {
	Name  string `json:"name,omitempty"`
	Email string `json:"email,omitempty"`
}

type Remote struct {
	Type     string   `json:"type,omitempty"`
	AnyOneOf []string `json:"any_one_of,omitempty"`
}

type mappingRoot struct {
	Mapping *Mapping `json:"mapping"`
}

type mappingsRoot struct {
	Mapping []Mapping `json:"mappings"`
	Links   *Links    `json:"links"`
}

type MappingCreateRequest struct {
	Mapping MappingCreateRequestPart `json:"mapping"`
}

type MappingUpdateRequest struct {
	Mapping MappingUpdateRequestPart `json:"mapping"`
}

type MappingUpdateRequestPart struct {
	// path attributes

	ID string `json:"-"`

	// body attributes

	Rules []*Rule `json:"rules"`
}

type MappingCreateRequestPart struct {
	// path attributes

	ID string `json:"-"`

	// body attributes

	Rules []*Rule `json:"rules"`
}

func (s MappingsServiceOp) List(opt *ListOptions) ([]Mapping, *Response, error) {
	path := mappingBasePath
	path, err := addOptions(path, opt)
	if err != nil {
		return nil, nil, err
	}

	req, err := s.client.NewRequest("GET", path, nil)
	if err != nil {
		return nil, nil, err
	}

	root := new(mappingsRoot)
	resp, err := s.client.Do(req, root)
	if err != nil {
		return nil, resp, err
	}
	if l := root.Links; l != nil {
		resp.Links = l
	}

	return root.Mapping, resp, err
}

func (s MappingsServiceOp) Get(id string) (*Mapping, *Response, error) {
	if len(id) < 1 {
		return nil, nil, NewArgError("id", "cannot be empty")
	}

	path := fmt.Sprintf("%s/%s", mappingBasePath, id)

	req, err := s.client.NewRequest("GET", path, nil)
	if err != nil {
		return nil, nil, err
	}

	root := new(mappingRoot)
	resp, err := s.client.Do(req, root)
	if err != nil {
		return nil, resp, err
	}

	return root.Mapping, resp, err
}

func (s *MappingsServiceOp) Create(createRequest *MappingCreateRequest) (*Mapping, *Response, error) {
	if createRequest == nil {
		return nil, nil, NewArgError("createRequest", "cannot be nil")
	}

	path := fmt.Sprintf("%s/%s", mappingBasePath, createRequest.Mapping.ID)

	req, err := s.client.NewRequest(http.MethodPut, path, createRequest)
	if err != nil {
		return nil, nil, err
	}

	root := new(mappingRoot)
	resp, err := s.client.Do(req, root)
	if err != nil {
		return nil, resp, err
	}
	return root.Mapping, resp, err
}

func (s *MappingsServiceOp) Update(updateRequest *MappingUpdateRequest) (*Mapping, *Response, error) {
	if updateRequest == nil {
		return nil, nil, NewArgError("updateRequest", "cannot be nil")
	}

	path := fmt.Sprintf("%s/%s", mappingBasePath, updateRequest.Mapping.ID)

	req, err := s.client.NewRequest(http.MethodPatch, path, updateRequest)
	if err != nil {
		return nil, nil, err
	}

	root := new(mappingRoot)
	resp, err := s.client.Do(req, root)
	if err != nil {
		return nil, resp, err
	}
	return root.Mapping, resp, err
}

func (s *MappingsServiceOp) Delete(name string) (*Response, error) {
	if len(name) < 1 {
		return nil, NewArgError("name", "cannot be an empty string")
	}

	path := fmt.Sprintf("%s/%s", mappingBasePath, name)

	req, err := s.client.NewRequest(http.MethodDelete, path, nil)
	if err != nil {
		return nil, err
	}

	resp, err := s.client.Do(req, nil)

	return resp, err
}

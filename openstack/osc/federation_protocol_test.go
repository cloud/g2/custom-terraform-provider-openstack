package osc

import (
	"encoding/json"
	"fmt"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"net/http"
	"testing"
)

func Test_simple_get_all_federation_protocols(t *testing.T) {
	setup()
	defer teardown()

	mux.HandleFunc("/v3/OS-FEDERATION/identity_providers/test/protocols", func(w http.ResponseWriter, r *http.Request) {
		testMethod(t, r, http.MethodGet)
		fmt.Fprint(w, `{
    "protocols": [
        {
            "id": "openid",
            "idp_id": "login.e-infra.cz",
            "mapping_id": "login_e_infra_cz_mapping",
			"remote_id_attribute": null
        },
		{
            "id": "saml2",
            "idp_id": "saml2.e-infra.cz",
            "mapping_id": "login_e_infra_cz_mapping_2",
			"remote_id_attribute": null
        }
    ],
    "links": {
        "next": null,
        "previous": null,
        "self": "http://example.com/identity/v3/OS-FEDERATION/identity_providers/test/protocols"
    }
}`)
	})

	protocols, _, err := client.FederationProtocols.List("test", nil)
	require.NoError(t, err)

	expectedProtocols := []FederationProtocol{
		{ID: "openid", IdentityProvider: "login.e-infra.cz", Mapping: "login_e_infra_cz_mapping"},
		{ID: "saml2", IdentityProvider: "saml2.e-infra.cz", Mapping: "login_e_infra_cz_mapping_2"},
	}

	assert.Equal(t, expectedProtocols, protocols)
}

func Test_get_single_protocol(t *testing.T) {
	setup()
	defer teardown()

	mux.HandleFunc("/v3/OS-FEDERATION/identity_providers/ACME/protocols/openid", func(w http.ResponseWriter, r *http.Request) {
		testMethod(t, r, http.MethodGet)
		fmt.Fprint(w, `{
    "protocol": {
        "id": "openid",
		"idp_id": "login.e-infra.cz",
		"mapping_id": "login_e_infra_cz_mapping",
		"remote_id_attribute": null
    }
}`)
	})

	protocol, _, err := client.FederationProtocols.Get("ACME", "openid")
	require.NoError(t, err)

	expectedProtocol := &FederationProtocol{
		IdentityProvider: "login.e-infra.cz",
		ID:               "openid",
		Mapping:          "login_e_infra_cz_mapping",
	}

	assert.Equal(t, expectedProtocol, protocol)
}

func Test_simple_create_federation_protocol(t *testing.T) {
	setup()
	defer teardown()

	createRequest := &FederationProtocolCreateRequest{
		FederationProtocol: FederationProtocolCreateRequestPart{
			IdentityProvider: "ACME",
			ID:               "TEST",
			Mapping:          "TEST-MAPPING",
		},
	}

	mux.HandleFunc("/v3/OS-FEDERATION/identity_providers/ACME/protocols/TEST", func(w http.ResponseWriter, r *http.Request) {
		v := new(FederationProtocolCreateRequest)
		err := json.NewDecoder(r.Body).Decode(v)
		if err != nil {
			t.Fatal(err)
		}

		testMethod(t, r, http.MethodPut)
		//assert.Equal(t, createRequest, v)

		fmt.Fprint(w, `{
    "protocol": {
		"id": "TEST",
		"idp_id": "ACME",
		"mapping_id": "TEST-MAPPING",
		"remote_id_attribute": null
    }
}`)
	})

	protocol, _, err := client.FederationProtocols.Create(createRequest)
	require.NoError(t, err)

	expected := &FederationProtocol{
		IdentityProvider: "ACME",
		ID:               "TEST",
		Mapping:          "TEST-MAPPING",
	}

	assert.Equal(t, expected, protocol)
}

//func TestDomains_CreateShouldThrowWithoutRequest(t *testing.T) {
//	setup()
//	defer teardown()
//
//	_, _, err := client.Domains.Create(nil)
//	require.Error(t, err)
//}
//
//func TestDomains_Destroy(t *testing.T) {
//	setup()
//	defer teardown()
//
//	mux.HandleFunc("/v3/domains/10", func(w http.ResponseWriter, r *http.Request) {
//		testMethod(t, r, http.MethodDelete)
//	})
//
//	_, err := client.Domains.Delete("10")
//
//	assert.NoError(t, err)
//}
